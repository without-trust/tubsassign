/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet die Level-Methode.
 * 
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Level {
    /**
     * the height
     */
    private int height; //Hoehe

    /**
     * the length
     */
    private int length; //Breite

    /**
     * the map
     */
    private char[][] map; //Karte

    /**
     * the visible area
     */
    private boolean[][] explored; //Sichtbarkeitsbereich

    /**
     * the players current x position
     */
    private int x;  //x-Position des Spielers

    /**
     * the players current y position
     */
    private int y;  //y-Position des Spielers

    /**
     * the shops
     */
    private Shop s;
    
    
    /**
     * Konstruktor
     *
     * @param mapData zweidimensionales Kartenarray
     */
    public Level(char[][] mapData) {
        this.height = mapData.length;
        this.length = mapData[0].length;
        this.map = mapData; 
        this.explored = new boolean[this.height][this.length];

        System.out.println("Generating shop...");
        this.s = new Shop();
        System.out.println("Shop generated...");
        
        //Startposition bestimmen, explored definieren
        for (int i = 0; i < this.height; i++) { //Hoehe bzw y
            for (int j = 0; j < this.length; j++) {  //Breite bzw. x
                this.explored[i][j] = false;
                if (this.map[i][j] == 'S') {
                    this.y = i;
                    this.x = j;
                } // end of if
            } // end of for
        } // end of for
        
        //Felder um das Startfeld sichtbar machen
        this.explored[this.y][this.x] = true;
        for (int i = 0; this.map[this.y][this.x - i] != '#'; i++) {  //links
            this.explored[this.y][this.x - i - 1] = true;
            this.explored[this.y + 1][this.x - i - 1] = true;
            this.explored[this.y - 1][this.x - i - 1] = true;
        } // end of for
        for (int i = 0; this.map[this.y][this.x + i] != '#'; i++) {  //rechts
            this.explored[this.y][this.x + i + 1] = true;
            this.explored[this.y + 1][this.x + i + 1] = true;
            this.explored[this.y - 1][this.x + i + 1] = true;
        } // end of for
        for (int i = 0; this.map[this.y - i][this.x] != '#'; i++) {  //oben
            this.explored[this.y - i - 1][this.x] = true;
            this.explored[this.y - i - 1][this.x - 1] = true;
            this.explored[this.y - i - 1][this.x + 1] = true;
        } // end of for
        for (int i = 0; this.map[this.y + i][this.x] != '#'; i++) {  //unten
            this.explored[this.y + i + 1][this.x] = true;
            this.explored[this.y + i + 1][this.x - 1] = true;
            this.explored[this.y + i + 1][this.x + 1] = true;
        } // end of for              
    }
    
    
    /**
     * Ausgabe des Labyrinths
     */
    public void output() {
        //Moeglichkeiten
        this.possibleMoves(); 
        
        //aktuelles Feld merken
        char c = this.map[this.y][this.x];
        
        //Spielerposition einfuegen           
        this.map[this.y][this.x] = (char) (64);
        
        //Ausgabe
        String s = "";
        for (int i = 0; i < this.height; i++) { //Hoehe bzw y
            for (int j = 0; j < this.length; j++) {  //Breite bzw. x
                if (this.explored[i][j]) {
                    if (this.map[i][j] == 'B') {
                        s += ". ";
                    } else {
                        s += this.map[i][j] + " "; 
                    } // end of if-else                    
                } else {
                    s += "  ";
                } // end of if-else
            } // end of for
            System.out.println(s);
            s = "";
        } // end of for
        
        //urspruengliches Feld wieder einfuegen
        this.map[this.y][this.x] = c;
    }
    
    
    /**
     * Ausgabe der Bewegungsmoeglichkeiten
     */
    public void possibleMoves() {
        System.out.println(""
            + "I - Inventar\n"
            + "L - Questlog\n"
            + "T - Speichern\n");

        System.out.println("Moegliche Bewegungsrichtungen: ");
        if (this.testNorth()) {
            System.out.println("W - Norden ");
        } // end of if
        if (this.testEast()) {
            System.out.println("D - Osten ");
        } // end of if
        if (this.testSouth()) {
            System.out.println("S - Sueden ");
        } // end of if
        if (this.testWest()) {
            System.out.println("A - Westen ");
        } // end of if
    }
    
    /**
     * Test der Bewegung nach Norden
     * @return a gibt an, ob die Bewegung nach Norden moeglich ist
     */
    public boolean testNorth() {
        boolean a;
        if ((this.y == 0) || (this.map[this.y - 1][this.x] == '#')) {           //an der oberen Kante oder oben #
            a = false;
        } else {
            a = true;
        } // end of if-else
        return a;
    }
    
    /**
     * Test der Bewegung nach Osten
     * @return a gibt an, ob die Bewegung nach Osten moeglich ist
     */
    public boolean testEast() {
        boolean a;
        if ((this.x == this.length - 1) || (this.map[this.y][this.x + 1] == '#')) { //an der rechten Kante oder rechts #
            a = false;
        } else {
            a = true;
        } // end of if-else
        return a;        
    }

    /**
     * Test der Bewegung nach Sueden
     * @return a gibt an, ob die Bewegung nach Sueden moeglich ist
     */
    public boolean testSouth() {
        boolean a;
        if ((this.y == this.height - 1) || (this.map[this.y + 1][this.x] == '#')) { //an der unteren Kante oder unten #
            a = false;
        } else {
            a = true;
        } // end of if-else
        return a;        
    }

    /**
     * Test der Bewegung nach Westen
     * @return a gibt an, ob die Bewegung nach Westen moeglich ist
     */
    public boolean testWest() {
        boolean a;
        if ((this.x == 0) || (this.map[this.y][this.x - 1] == '#')) {           //an der linken Kante oder links #
            a = false;
        } else {
            a = true;
        } // end of if-else
        return a;        
    }

    /**
     * Bewegungsablauf
     *
     * @param c gibt die Bewegungsrichtung an
     * @return b gibt das Feld an, auf dem man nach der Bewegung gelandet ist, bzw 'N', wenn nicht bewegt wurde
     */
    public char move(char c) {      
        boolean a = false;
        if (c < 58) {
            if (c == '8') {
                c = 'W';
            } else if (c == '4') {
                c = 'D';
            } else if (c == '5') {
                c = 'S';
            } else if (c == '6') {
                c = 'A';
            } // end of if
        } // end of if
        switch (c) {
            case 'W': 
                if (this.testNorth()) {
                    this.y--;
                    a = true;
                    for (int i = 0; this.map[this.y][this.x - i] != '#'; i++) { //links
                        this.explored[this.y][this.x - i - 1] = true;
                        this.explored[this.y + 1][this.x - i - 1] = true;
                        this.explored[this.y - 1][this.x - i - 1] = true;
                    } // end of for
                    for (int i = 0; this.map[this.y][this.x + i] != '#'; i++) {  //rechts
                        this.explored[this.y][this.x + i + 1] = true;
                        this.explored[this.y + 1][this.x + i + 1] = true;
                        this.explored[this.y - 1][this.x + i + 1] = true;
                    } // end of for
                }
                break;        
                
            case 'D': 
                if (this.testEast()) {
                    this.x++;
                    a = true;
                    for (int i = 0; this.map[this.y - i][this.x] != '#'; i++) {  //oben
                        this.explored[this.y - i - 1][this.x] = true;
                        this.explored[this.y - i - 1][this.x - 1] = true;
                        this.explored[this.y - i - 1][this.x + 1] = true;
                    } // end of for
                    for (int i = 0; this.map[this.y + i][this.x] != '#'; i++) {  //unten
                        this.explored[this.y + i + 1][this.x] = true;
                        this.explored[this.y + i + 1][this.x - 1] = true;
                        this.explored[this.y + i + 1][this.x + 1] = true;
                    } // end of for  
                } // end of if
                break;
                
            case 'S':
                if (this.testSouth()) {
                    this.y++;
                    a = true;
                    for (int i = 0; this.map[this.y][this.x - i] != '#'; i++) {  //links
                        this.explored[this.y][this.x - i - 1] = true;
                        this.explored[this.y + 1][this.x - i - 1] = true;
                        this.explored[this.y - 1][this.x - i - 1] = true;
                    } // end of for
                    for (int i = 0; this.map[this.y][this.x + i] != '#'; i++) {  //rechts
                        this.explored[this.y][this.x + i + 1] = true;
                        this.explored[this.y + 1][this.x + i + 1] = true;
                        this.explored[this.y - 1][this.x + i + 1] = true;
                    } // end of for 
                } // end of if
                break;
                
            case 'A':
                if (this.testWest()) {
                    this.x--;
                    a = true;
                    for (int i = 0; this.map[this.y - i][this.x] != '#'; i++) {  //oben
                        this.explored[this.y - i - 1][this.x] = true;
                        this.explored[this.y - i - 1][this.x - 1] = true;
                        this.explored[this.y - i - 1][this.x + 1] = true;
                    } // end of for
                    for (int i = 0; this.map[this.y + i][this.x] != '#'; i++) {  //unten
                        this.explored[this.y + i + 1][this.x] = true;
                        this.explored[this.y + i + 1][this.x - 1] = true;
                        this.explored[this.y + i + 1][this.x + 1] = true;
                    } // end of for 
                } // end of if
                break;

            default:

            
        } // end of switch
        
        //Aktion
        char b;
        if (a) {
            b = this.map[this.y][this.x];
            if (b == 'T') {
                //Aus Grossbuchstaben Kleinbuchstaben machen
                this.map[this.y][this.x] = (char) ((int) (this.map[this.y][this.x]) + 32); 
            }
        } else if (c == 'I') {
            b = 'I';
        } else {
            b = 'N';
        } // end of if-else
        
        return b;
    }

    /**
     * Verwendung des Shops
     *
     *@param p Kunde des Shops
     */
    public void useShop(Player p) {
        s.use(p);
    }
    
  //  /**
  //   * Ignorieren eines Brunnens
  //   */
  //  public void skipFountain() {
  //      if (this.map[this.y][this.x] == 'o') {
  //          this.map[this.y][this.x] = 'O';
  //      } // end of if
  //  }
}
