/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet das Interface MazeGenerator.
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 2a
 * @author Steffen Richter 4388227 Gruppe 2a
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
public interface MazeGenerator {

    /**
     *wall
     */
    char WALL = '#';

    /**
     *free space
     */
    char FREE = '.';

    /**
     *start field
     */
    char START = 'S';

    /**
     *battle
     */
    char BATTLE = 'B';

    /**
     *smith
     */
    char SMITHY = 'T';

    /**
     *well
     */
    char WELL = 'O';

    /**
     *goal
     */
    char GOAL = 'Z';

    /**
     *@return map
     *
     *@param height height
     *@param width width
     */
    char[][] generate(int height, int width);
}
