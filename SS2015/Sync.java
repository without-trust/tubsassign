/**
 * Sync battle gui with the rest
 * @author Name 0000000 Gruppe 5b
 */
public class Sync {

    /** locking object */
    private static final Object LOCK = new Object();

    /**
     * battle finished
     */
    public static void battleFinished() {
        synchronized (LOCK) {
            LOCK.notifyAll();
        }
    }

    /**
     * wait for battle end
     */
    public static void waitForBattleEnd() {
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException ignored) {
                int i = 66;
            }
        }
    }
}
