/**
 * A secret to force Items, Quests etc. to be cloneable
 *
 * "Niemand hat die Absicht eine Armee von Klonkriegern zu errichten!"
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 *
 * @param <T>
 */
public interface Secret<T> extends Comparable<T> {
    /**
     * method for creating clones
     * @return the clone
     */
    T clone();
}
