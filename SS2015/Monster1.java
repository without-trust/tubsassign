/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet die Monster1-Methode (Werwolf).
 * 
 * @author Eva Vanessa Bolle 4528650 Gruppe 2a
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Monster1 extends Monster {
    
    /**
     * Konstruktor
     * @param maxHp Die maximale Anzahl an Lebenspunkten
     * @param hitChance Die Trefferwahrscheinlichkeit     
     */
    public Monster1(int maxHp, double hitChance) {           
    
        setMaxHp(maxHp);
        setHp(maxHp);              
        setHitChance(hitChance); 
        setName("Werwolf");
        setAtk(getRate());
        setInventory(new GenericLinkedList<Item>());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            this.getInventory().insert(new Item());
        }
        this.setGold((int) (Math.random() * 133.7));
    }
    
    /**
     * Konstruktor (Standardwerte fuer das Monster) 
     */
    public Monster1() {
        setMaxHp(125);
        this.setHp(this.getMaxHp());
        this.setHitChance(0.5);
        this.setName("Werwolf");
        setAtk(getRate());
        setInventory(new GenericLinkedList<Item>());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            this.getInventory().insert(new Item());
        }
        this.setGold((int) (Math.random() * 133.7));
    }  
}

