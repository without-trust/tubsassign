import java.util.Scanner;

/**
 * The type Level.
 * @author Name 0000000 Gruppe 0
 */
public class Level {
    /**
     * The constant PLAIN.
     */
    public static final char PLAIN = '.';
    /**
     * The constant PLAYER_CHAR.
     */
    public static final char PLAYER_CHAR = 'P';
    /**
     * The constant FOUNTAIN.
     */
    public static final char FOUNTAIN = 'O';
    /**
     * The constant SMITHY.
     */
    public static final char SMITHY = 'T';
    /**
     * The constant BATTLE.
     */
    public static final char BATTLE = 'B';
    /**
     * The constant GOAL.
     */
    public static final char GOAL = 'Z';
    /**
     * The constant START.
     */
    public static final char START = 'S';   
    /**
     * The constant ATKBONUS.
     */
    private static final int ATKBONUS = 10;
    
    /**
     * The Map data.
     */
    private char[][] mapData;
    /**
     * The Player x coordinate.
     */
    private int playerX;
    /**
     * The Player y coordinate.
     */
    private int playerY;

    /**
     * Instantiates a new Level.
     *
     * @param mapData the map data
     */
    public Level(char[][] mapData) {
        if (mapData.length < 3 || mapData[0].length < 3) {
            throw new IllegalArgumentException("Invalid Map Data");
        }
        this.mapData = mapData;
        if (!this.findStart()) {
            throw new IllegalArgumentException("Invalid Map Data: No starting position");
        }
    }

    /**
     * Find start.
     *
     * @return true, wenn die Startposition gefunden wuerde
     */
    private boolean findStart() {
        for (int y = 0; y < this.mapData.length; y++) {
            for (int x = 0; x < this.mapData[0].length; x++) {
                if (this.mapData[y][x] == START) {
                    this.playerX = x;
                    this.playerY = y;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * To string.
     *
     * @return the string
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < this.mapData.length; ++y) {
            for (int x = 0; x < this.mapData[0].length; ++x) {
                if (y == this.playerY && x == this.playerX) {
                    sb.append(PLAYER_CHAR);
                } else {
                    sb.append(this.mapData[y][x]);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Can move.
     *
     * @param c the direction
     * @return true, wenn die Richtung möglich ist
     */
    public boolean canMove(char c) {
        switch (c) {
            case 'n':
                return this.canMoveUp();
            case 's':
                return this.canMoveDown();
            case 'o':
                return this.canMoveRight();
            case 'w':
                return this.canMoveLeft();
            default:
                return false;
        }
    }

    /**
     * Move void.
     *
     * @param c the direction
     */
    public void move(char c) {
        switch (c) {
            case 'n':
                this.moveUp();
                break;
            case 's':
                this.moveDown();
                break;
            case 'o':
                this.moveRight();
                break;
            case 'w':
                this.moveLeft();
                break;
            default:
                break;
        }
    }


    /**
     * Is walkable position.
     *
     * @param x the x coordinate
     * @param y the y coordinate
     * @return true, wenn das Feld x,y begehbar ist
     */
    public boolean isWalkablePosition(int x, int y) {
        return (y >= 0) && (x >= 0) && (y < this.mapData.length) && (x < this.mapData[0].length) 
            && (this.mapData[y][x] == PLAIN || this.mapData[y][x] == FOUNTAIN || this.mapData[y][x] == SMITHY
                || this.mapData[y][x] == BATTLE || this.mapData[y][x] == GOAL || this.mapData[y][x] == START);
    }

    /**
     * Can move up.
     *
     * @return true, wenn mögliche Bewegung
     */
    public boolean canMoveUp() {
        return this.isWalkablePosition(this.playerX, this.playerY - 1);
    }

    /**
     * Can move down.
     *
     * @return true, wenn mögliche Bewegung
     */
    public boolean canMoveDown() {
        return this.isWalkablePosition(this.playerX, this.playerY + 1);
    }

    /**
     * Can move left.
     *
     * @return true, wenn mögliche Bewegung
     */
    public boolean canMoveLeft() {
        return this.isWalkablePosition(this.playerX - 1, this.playerY);
    }

    /**
     * Can move right.
     *
     * @return true, wenn mögliche Bewegung
     */
    public boolean canMoveRight() {
        return this.isWalkablePosition(this.playerX + 1, this.playerY);
    }

    /**
     * Move up.
     */
    public void moveUp() {
        if (this.canMoveUp()) {
            this.playerY--;
        }
    }

    /**
     * Move down.
     */
    public void moveDown() {
        if (this.canMoveDown()) {
            this.playerY++;
        }
    }

    /**
     * Move left.
     */
    public void moveLeft() {
        if (this.canMoveLeft()) {
            this.playerX--;
        }
    }

    /**
     * Move right.
     */
    public void moveRight() {
        if (this.canMoveRight()) {
            this.playerX++;
        }
    }

    /**
     * Show prompt.
     */
    public void showPrompt() {
        System.out.println("------------------------------");
        if (this.canMoveUp()) {
            System.out.println("n -> Norden");
        }
        if (this.canMoveDown()) {
            System.out.println("s -> Sueden");
        }
        if (this.canMoveRight()) {
            System.out.println("o -> Osten");
        }
        if (this.canMoveLeft()) {
            System.out.println("w -> Westen");
        }
        System.out.println("------------------------------");
        System.out.print("Richtung? ");
    }

    /**
     * Gets field.
     *
     * @return the field
     */
    private char getField() {
        return this.mapData[this.playerY][this.playerX];
    }

    /**
     * Clear field.
     */
    private void clearField() {
        char field = this.getField();
        if (field == SMITHY || field == FOUNTAIN || field == BATTLE) {
            this.mapData[this.playerY][this.playerX] = PLAIN;
        }
    }

    /**
     * Handle current field event.
     *
     * @param p the player
     */
    public void handleCurrentFieldEvent(Player p) {
        char field = this.getField();
        switch (field) {
            case Level.SMITHY:
                p.setAtk(p.getAtk() + ATKBONUS);
                System.out.printf("Die ATK des Spielers wurde um %d erhöht.%n", ATKBONUS);
                break;
            case Level.FOUNTAIN:
                p.setHp(p.getMaxHp());
                System.out.println("Spieler wurde vollständig geheilt!");
                break;
            case Level.BATTLE:
                this.startBattle(p);
                break;
            case Level.GOAL:
                System.out.println("Herzlichen Glückwunsch! Sie haben gewonnen!");
                System.exit(0);
                break;
            default:
                break;
        }
        this.clearField();
    }

    /**
     * Random monster.
     *
     * @return the monster
     */
    private static Monster randomMonster() {
        Monster[] monsterFarm = {
            new Monster(),
            new ResistantMonster(),
            new WaitingMonster()
        };

        double bucketSize = 1.0 / monsterFarm.length;
        double bucket = Math.random() / bucketSize;
        int selectedMonster = (int) Math.floor(bucket);
        return monsterFarm[selectedMonster];
    }

    /**
     * Start battle.
     *
     * @param p the p
     */
    public void startBattle(Player p) {
        Character m = randomMonster();

        Scanner sc = new Scanner(System.in);
        
        System.out.println("                 Kampf Start                    ");
        System.out.print(p);
        System.out.print(m);

        while (true) {
            System.out.println("------------------------------------------------");
            System.out.println("Mögliche Aktionen:");
            System.out.println("1 -> Angriff");
            System.out.printf("2 -> Item (%d verbleibend)%n", p.getRemainingItemUses());
            System.out.printf("3 -> Harter Schlag (%d AP, %d%% Selbstschaden)%n", 
                Player.HARD_HIT_COST, Player.HARD_HIT_SELF_DAMAGE_PERCENT);
            System.out.printf("4 -> Feuerball (%d AP)%n", Player.FIREBALL_COST);
            System.out.printf("5 -> ATK auswürfeln (%d AP)%n", Player.REROLL_COST);
            System.out.println("Welche Aktion?: ");
            System.out.println("------------------------------------------------");
            String aktion = sc.nextLine();
            int playerDamage;
            switch (aktion) {
                case "1":
                    playerDamage = p.attack(m);
                    if (playerDamage == -1) {
                        System.out.println("Spieler verfehlt!");
                    } else {
                        System.out.printf("Spieler trifft und macht %d Schaden!%n", playerDamage);
                    }
                    break;
                case "2":
                    if (p.heal()) {
                        System.out.println("Spieler heilt sich!");
                    } else {
                        System.out.println("Nicht genügend Heiltränke!");
                    }
                    break;
                case "3":
                    playerDamage = p.hardHit(m);
                    if (playerDamage != -1) {
                        System.out.println("Spieler schlägt hart zu!");
                        System.out.printf("Spieler verursacht %d Schaden!%n", playerDamage);
                        System.out.printf("Spieler verursacht %d Selbstschaden!%n", 
                            (int) (Player.HARD_HIT_SELF_DAMAGE_PERCENT / 100.0 * playerDamage));
                    } else {
                        System.out.println("Nicht genügend AP!");
                    }
                    break;
                case "4":
                    playerDamage = p.fireball(m);
                    if (playerDamage != -1) {
                        System.out.println("Spieler schießt einen Feuerball!");
                        System.out.printf("Spieler verursacht %d Schaden!%n", playerDamage);
                    } else {
                        System.out.println("Nicht genügend AP!");
                    }
                    break;
                case "5":
                    if (p.reroll()) {
                        System.out.println("ATK neu ausgewürfelt!");
                        System.out.print("Neue Statuswerte: ");
                        System.out.print(p);
                    } else {
                        System.out.println("Nicht genügend AP!");
                    }
                    break;
                default:
                    System.out.println("Fehlerhafte Aktion!");
                    continue;
            }

            if (p.isDefeated()) {
                System.out.println("Game Over!");
                System.exit(0);
            } else if (m.isDefeated()) {
                System.out.println("Spieler gewinnt!");
                break;
            }

            System.out.print(p);
            System.out.print(m);

            System.out.println("Monster greift an!");
            int monsterDamage = m.attack(p);
            if (monsterDamage == -1) {
                System.out.println("Monster verfehlt!");
            } else if (monsterDamage == -2) {
                System.out.println("Monster tut nichts.");
            } else {
                System.out.printf("Monster trifft und macht %d Schaden!%n", monsterDamage);
            }

            if (p.isDefeated()) {
                System.out.println("Game Over!");
                System.exit(0);
            }

            p.regenerateAp();

            System.out.print(p);
            System.out.print(m);
        }
    }

}
