import java.io.Serializable;
/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet die Entity (ehem. Character) - Methode.
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Entity implements Serializable {
    /** inventory */
    private GenericLinkedList<Item> inventory;

    /** gold */
    private int gold;

    /** the maximum value for maxHp */
    private int maxHp;

    /** current hp */
    private int hp;

    /** the strength of an atk */
    private int atk;

    /** the chance to hit */
    private double hitChance; //Die Trefferwahrscheinlichkeit

    /** name of entity*/
    private String name;

    /** modifyer for atk value (later depending on hp) */
    private double rate = 1.0;

    /** @return hp*/
    public int getHp() { 
        return this.hp; 
    }
    /** @param hp HP */
    public void setHp(int hp) { 
        this.hp = hp; 
    }
    
    /** @return atk */
    public int getAtk() { 
        return this.atk; 
    }
    /** @param rate modifyer that describes the rate of hp used as atk */
    public void setAtk(double rate) { 
        this.atk = (int) (rate * this.hp / 5); 
    }

    /** @return hitchance*/
    public double getHitChance() { 
        return this.hitChance; 
    }
    /** @param hitChance chnace to hit */
    public void setHitChance(double hitChance) { 
        this.hitChance = hitChance; 
    }
    
    /** @return maxHp*/
    public int getMaxHp() { 
        return this.maxHp; 
    }
    /** @param maxHp maximal HP */
    public void setMaxHp(int maxHp) { 
        this.maxHp = maxHp; 
    }
    
    /** @return amount of gold */
    public int getGold() { 
        return this.gold; 
    }
    /** @param g gold */
    public void setGold(int g) { 
        this.gold = g; 
    }

    /** @return rate */
    public double getRate() { 
        return this.rate; 
    }
    /** @param rate the rate */
    public void setRate(double rate) { 
        this.rate = rate; 
    }

    /** @return inventory */
    public GenericLinkedList<Item> getInventory() { 
        return this.inventory;
    }
    /** @param inv new inventory */
    public void setInventory(GenericLinkedList<Item> inv) { 
        this.inventory = inv;
    }

    /** @return name of entity */
    public String getName() { 
        return this.name;
    }

    /** @param name new name */
    public void setName(String name) { 
        this.name = name; 
    }
    

    /**
     * Gibt zurueck, ob das Wesen besiegt ist
     * @return Booleanwert, der angibt, ob das Wesen besiegt ist
     */
    public boolean isDefeated() {
        return (this.hp == 0);
    }
    
    /**
     * fuegt Wesen Schaden zu
     * @param damage erlittener Schaden 
     */
    public void takeDamage(int damage) {
        this.hp = this.hp - damage;
        if (this.hp < 0) {
            this.hp = 0;
        }    
        this.setAtk(this.rate);
    }

    /**
     * implements toString method
     * @return String, der die aktuellen Statuswerte des Monsters enthaelt
     */
    public String toString() {   
        return (this.name + " -- HP " + this.hp + " -- ATK " + this.atk);
    }
}
