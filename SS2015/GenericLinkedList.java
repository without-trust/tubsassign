import java.io.Serializable;

/**
 * Yet another linked list implementation done in Java
 * This one contains recursion to do its job
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 *
 * @param <T> java generic wildcard
 */
public class GenericLinkedList<T extends Comparable<T>> implements Serializable {

    /**
     * Das aktuelle T
     */
    private T item;

    /**
     * Die nachfolgende Liste
     */
    private GenericLinkedList<T> next;

    /**
     * Konstruktor, erzeugt leere Liste
     */
    public GenericLinkedList() {
        this.item = null;
        this.next = null;
    }

    /**
     * Ueberprueft ob die Liste leer ist
     *
     * @return true, Liste ist leer
     */
    public boolean isEmpty() {
        return this.next == null;
    }

    /**
     * Gibt die Laenge der Liste zurück
     *
     * @return die Laenge
     */
    public int length() {
        if (this.isEmpty()) {
            return 0;
        } else {
            return 1 + this.next.length();
        }
    }

    /**
     * Prueft ob ein T in der Liste ist
     *
     * @param x das T
     * @return true, x ist in der Liste enthalten
     */
    public boolean isInList(T x) {
        if (x.equals(this.item)) {
            return true;
        }
        
        if (this.isEmpty()) {
            return false;
        } else {
            return this.next.isInList(x);
        }
    }

    /**
     * Gibt das erste T der Liste zurueck
     *
     * @return das erste T
     * @throws IllegalStateException wenn die Liste leer ist
     */
    public T firstItem() throws IllegalStateException {
        if (this.isEmpty()) {
            throw new IllegalStateException();
            //return null;
        } else {
            return this.next.item;
        }
    }

    /**
     * Gibt das i-te T der Liste zurueck
     *
     * @param i der Index
     * @return das i-te T
     * @throws IndexOutOfBoundsException wenn i < 0 oder  i >= length()
     */
    public T getItem(int i) throws IndexOutOfBoundsException {
        if (i < 0 || i >= this.length()) {
            throw new IndexOutOfBoundsException();
        }

        if (i == 0) {
            return this.next.item;
        } else {
            return this.next.getItem(i - 1);
        }
    }

    /**
     * Fuegt ein Element sortiert in die Liste ein
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericLinkedList<T> insert(T x) {
        if (this.isEmpty() || x.compareTo(this.next.item) <= 0) {
            return this.insertHELPER(x);
        } else {
            return this.next.insert(x);
        }
    }

    /**
     * private helper function, used to insert T right after the current one
     *
     * @param x T to be added
     * @return the List
     */
    private GenericLinkedList<T> insertHELPER(T x) {
        GenericLinkedList<T> tmp = new GenericLinkedList<T>();
        tmp.item = x;
        tmp.next = this.next;
        this.next = tmp;
        return this;
    }

    /**
     * Fuegt ein Element an das Ende der Liste ein
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericLinkedList<T> append(T x) {
        if (this.isEmpty()) {
            return this.insertHELPER(x);
        } else {
            return this.next.append(x);
        }
    }

    /**
     * Loescht das erste vorkommen des Ts x
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericLinkedList<T> delete(T x) {
        if (!this.isEmpty()) {
            if (this.next.item.equals(x)) {
                return this.delete();
            } else {
                return this.next.delete(x);
            }
        }
        return this;
    }

    /**
     * Loescht das erste Element der Liste
     *
     * @return die geanderte Liste
     */
    public GenericLinkedList<T> delete() {
        this.next = this.next.next;
        return this;
    }

    /**
     * input int to index int
     * @param i index of item
     * @return Index
     */
    public int inputToIndex(int i) {
        int j = 0;
        int k = 0;
        for (k = 0; k < this.length() && j < i; k++) {
            if (!(k + 1 < this.length() && this.getItem(k).equals(this.getItem(k + 1)))) {
                j++;
            }
        }
        return k;
    }
    /**
     *macht aus der Liste nen kurzen String
     *(gezählte Elemente)
     *
     *@return String
     */
    public String toShortString() {
        String s = "";
        int k = 0;
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            int j = 1;  //Nice Idea, works, but the Index get messed up.. And it's kinda annoying to fix
            for (int i = 0; i < this.length(); i++) {
                if (i + 1 < this.length() && this.getItem(i).equals(this.getItem(i + 1))) {
                    j++;
                } else {
                    s = s + k + " - " + this.getItem(i).toString() + " (" + j + "x)\n";
                    j = 1;
                    k++;
                }
            }
        }
        return s;
    }

    /**
     *macht aus der Liste nen String
     *
     *@return String
     */
    public String toString() {
        String s = "";
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            for (int i = 0; i < this.length(); i++) {
                s = s + i + " - " + this.getItem(i).toString() + "\n";
            }
        }
        return s;
    }


    /**
     * main method for testing purposes
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        GenericLinkedList<Quest> l = new GenericLinkedList<Quest>();


        int citm = 5;
        Quest[] itm = new Quest[citm];


        System.out.println("Items to add...");
        for (int i = 0; i < citm; i++) {
            itm[i] = new Quest();
            //itm[i].setName("Item No. " + ((int) (Math.random() * 100)) );
            System.out.println(i + ". " + itm[i]);
        }
        
        
        System.out.println("\nList stuff...");

        for (int i = 0; i < citm; i++) {
            l.insert(itm[i]);
            System.out.println("length: " + l.length());
            System.out.println(l);
        }


        System.out.println("== GET FIRST ITEM");
        System.out.println(l.firstItem());


        System.out.println("== GET ITEM BY INDEX");
        for (int i = 0; i < citm; i++) {
            System.out.println("getItem(" + i + ") -> " + l.getItem(i));
        }

        System.out.println("== DELETE FIRST");
        l.delete();
        System.out.println(l);

        System.out.println("== CHECK IF IN LIST");
        for (int i = 0; i < citm; i++) {
            if (l.isInList(itm[i])) {
                System.out.println("  found   \"" + itm[i] + "\"");
            } else {
                System.out.println("not found \"" + itm[i] + "\"");
            }
        }

        System.out.println("== DELETE ALL");
        for (int i = 0; i < citm; i++) {
            System.out.println("delete(" + itm[i] + ")");
            l.delete(itm[i]);
            System.out.println(l);
        }

        System.out.println("== END OF TEST");
    }
}
