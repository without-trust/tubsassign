import java.io.*;

/**
 * Dies ist die aktuelle Pflichtaufgabe.
 *
 * Diese Datei beinhaltet die Player-Methode.
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 2a
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
  
class Player extends Entity {
    /**
     * quests
     */
    private GenericLinkedList<Quest> quests;

    /**
     *strength of a potion
     */
    private int healingPower;  //Die Staerke eines Erste-Hilfe-Paketes    

    /**
     *number of potions
     */
    private int remainingItemUses;  //Die Anzahl an verbleibenden Erste-Hilfe-Paketen

    /**
     *current ap value
     */
    private int ap;  //Der aktuelle AP-Wert   

    /**
     *max value for ap
     */
    private int maxAp; //Die maximale Anzahl an Aktionspunkten    

    /**
     *how many ap are regenerated per round
     */
    private int apRegen; //Die Regeneration von AP pro Runde
    
    /**
     *max number of potions
     */
    private int maxItem;
    /**
    Konstruktor    
    @param maxHp max amount of hp
    @param healingPower strength of a potion
    @param remainingItemUses number of potions
    @param hitChance hit chance    
    */
    public Player(int maxHp, int healingPower, int remainingItemUses, double hitChance) {    
        this.setMaxHp(maxHp);
        this.setHp(maxHp);
        this.healingPower = healingPower;
        this.remainingItemUses = remainingItemUses;
        this.setHitChance(hitChance);
        this.maxAp = 60;
        this.ap = this.maxAp;
        this.apRegen = 10;
        this.setName("Spieler");
        this.maxItem = 8;
        setAtk(this.getRate());

        this.quests = new GenericLinkedList<Quest>();
        try {
            CSVHandler h = new CSVHandler("quest.csv");
            h.getQuests(this.quests);
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }

        setInventory(new GenericLinkedList<Item>());
        setGold(0);
    }
    
    /**    
    Konstruktor   
    @param maxHp Die maximale Anzahl an Heilungspunkten    
    @param healingPower Die Heilkraft eines Erste-Hilfe-Paketes  
    @param remainingItemUses Die Anzahl an verbleibenden Erste-Hilfe-Paketen
    @param hitChance Die Trefferwahrscheinlichkeit    
    @param maxAp Die maximale Anzahl an Aktionspunkten    
    @param apRegen Die Regeneration von AP pro Runde    
    */
    public Player(int maxHp, int healingPower, int remainingItemUses, 
        double hitChance, int maxAp, int apRegen) {          
        this.setMaxHp(maxHp);
        this.setHp(maxHp);
        this.healingPower = healingPower;        
        this.remainingItemUses = remainingItemUses;        
        this.setHitChance(hitChance);          
        this.maxAp = maxAp;        
        this.ap = maxAp;        
        this.apRegen = apRegen;
        this.setName("Spieler");
        this.maxItem = 10;
        setAtk(getRate());    
        
        this.quests = new GenericLinkedList<Quest>();
        try {
            CSVHandler h = new CSVHandler("quest.csv");
            h.getQuests(this.quests);
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }

        this.setInventory(new GenericLinkedList<Item>());
        setGold(0);
    }

    /**
    Konstruktor (Standartwerte)
    */
    public Player() {
        this.setMaxHp(125);
        this.setHp(this.getMaxHp());
        this.healingPower = 80;        
        this.remainingItemUses = this.maxItem;        
        this.setHitChance(0.5);      
        this.maxAp = 60;        
        this.ap = this.maxAp;        
        this.apRegen = 7;
        this.setName("Spieler");
        this.maxItem = 10;
        setAtk(getRate());              

        this.quests = new GenericLinkedList<Quest>();
        try {
            CSVHandler h = new CSVHandler("quest.csv");
            h.getQuests(this.quests);
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }

        this.setInventory(new GenericLinkedList<Item>());
        setGold(0);
    }
   
    /**
     * Angriff auf das Monster
     * @param monster Das angegriffene Monster
     * @return Integer-Wert, der bei Teffer den Schaden angibt, ansonsten -1
     */
    public int attack(Monster monster) {           
        double r = Math.random();  
        int d = -1;
        if (this.ap >= 20) {
            this.ap = this.ap - 20;
            if (r < this.getHitChance()) {           
                d = (int) ((0.0 + this.getAtk()) * (Math.random() + 1));                                       
                monster.takeDamage(d);            
                System.out.println("Monster wurde erfolgreich getroffen!");                                     
            } else {             
                //d = -1;                                    
                System.out.println("Monster wurde nicht getroffen!");                                 
            }      
        }
        return d;      
    }
    
    /**
     * Erster Spezialangriff bzw. Spezialfaehigkeit: Ausruhen (AP werden zu HP)
     * @return amount of HP restored (-1 if something went wrong)
     */
    public int specialattack1() {        
        if (this.ap < 40) {
            System.out.println("Du findest keine Ruhe!");
            return -1;
        } else {            
            this.ap = this.ap - 40;            
            this.setHp(this.getHp() + 20);             
            if (this.getHp() > this.getMaxHp()) {                 
                this.setHp(this.getMaxHp());
            }        
            setAtk(getRate());   
            System.out.println("Du fuehlst dich etwas frischer.");            
            return 20;
        }        
    }
    
    /**
     * Zweiter Spezialangriff(Revolver mit Silberkugeln): Angriff mit viel Schaden
     *
     * @param monster Angegriffenes Monster
     * @return Zugefuegter Schaden    
     */
    public int specialattack2(Monster monster) { 
        int d;        
        if (this.ap < 50) {            
            d = -1;            
            System.out.println("Du bist zu schwach fuer diese Attacke!");            
        } else {            
            this.ap = this.ap - 50;           
            double r = Math.random();   
            if (r < this.getHitChance()) {               
                d = (int) ((this.getAtk() * 1.5) * (Math.random() + 1));
                if (monster.getName().equals("Werwolf")) {
                    d = d * 2;
                }                                        
                monster.takeDamage(d);                
                System.out.println("Monster wurde erfolgreich getroffen!");                                         
            } else {                 
                d = -1;                                        
                System.out.println("Monster wurde nicht getroffen!");                                    
            }             
        }        
        return d;         
    }
    
    /**
     * Dritter Spezialangriff (Weihwasser) : Angriff mit hoher Trefferwahrscheinlichkeit, aber nur geringem Schaden
     * @param monster Angegriffenes Monster
     * @return Zugefuegter Schaden
     */
    public int specialattack3(Monster monster) { 
        int d;      
        if (this.ap < 30) {            
            d = -1;            
            System.out.println("Du bist zu schwach fuer diese Attacke!");            
        } else {           
            this.ap = this.ap - 30;            
            double r = Math.random();   
            if (r < (this.getHitChance() * 1.5)) {               
                d = (int) ((this.getAtk() * 0.8) * (Math.random() + 1)); 
                if (monster.getName().equals("Vampir")) {
                    d = d * 2;
                }                                        
                monster.takeDamage(d);                
                System.out.println("Monster wurde erfolgreich getroffen!");                                         
            } else {                 
                d = -1;                                        
                System.out.println("Monster wurde nicht getroffen!");                                     
            }             
        }        
        return d;         
    }
    
    /**
     * Verfuegbare Itemanzahl
     *
     * @return Anzahl der verfuegbaren Erste-Hilfe-Pakete
     */
    public int getRemainingItemUses() {         
        return (this.remainingItemUses);        
    }

    /**
     * Max items
     *
     * @return Max Items
     */
    public int getMaxItems() {
        return (this.maxItem);
    }

    /**
     *  getter for ap
     *  @return value of ap
     */
    public int getAp() {
        return this.ap;
    }

    /**
     * getter for maxAp
     * @return maxAp
     */
    public int getMaxAp() {
        return this.maxAp;
    }

    /** @return quests */
    GenericLinkedList<Quest> getQuests() { return this.quests; }
    /** @param quests */
    void setQuests(GenericLinkedList<Quest> quests) { this.quests = quests; }
    
    /**
    Verwendung von Erste-Hilfe-Paket
    @return Boolean-Wert, wenn etwas geheilt wurde
    */
    public boolean heal() {       
        if (this.remainingItemUses < 1) {            
            return false;            
        } else {          
            this.remainingItemUses--;  
            this.setHp(this.getHp() + this.healingPower);
            if (this.getHp() > this.getMaxHp()) {                  
                this.setHp(this.getMaxHp());                 
            }         
            setAtk(getRate());  
            return true;            
        }        
    }

    /**
     * checks wether or not the prequest for is done
     * @param q quest to check
     * @return is prequest done
     */
    public boolean isPrequestDone(Quest q) {
        if (q.getPrequest().equals("")) {
            return true;
        }
        
        int len = this.quests.length();
        for (int i = 0; i < len; i++) {
            Quest tmpq = this.quests.getItem(i);
            if (tmpq.getName().equals(q.getPrequest())) {
                return tmpq.isCompleted();
            }
        }

        return false;
    }

    /**
     * method to get quests that are possible for the player to accept
     * @return list of potential new quests
     */
    public GenericLinkedList<Quest> getNewQuests() {
        GenericLinkedList<Quest> nq = new GenericLinkedList<>();
        int len = this.quests.length();
        for (int i = 0; i < len; i++) {
            Quest tmpq = this.quests.getItem(i);
            
            if (!tmpq.isActive() && !tmpq.isCompleted() && this.isPrequestDone(tmpq)) {
                nq.insert(tmpq);
            }
        }

        return nq;
    }

    /**
     * quests that are already completed
     * @return list of completed quests
     */
    public GenericLinkedList<Quest> getCompletedQuests() {
        GenericLinkedList<Quest> cq = new GenericLinkedList<>();
        int len = this.quests.length();
        for (int i = 0; i < len; i++) {
            Quest tmpq = this.quests.getItem(i);
            
            if (tmpq.isCompleted()) {
                cq.insert(tmpq);
            }
        }

        return cq;
    }

    /**
     * list of all quests that are ready to be completed
     * @return list of quests to bring to your QB 
     */
    public GenericLinkedList<Quest> getItemCompletedQuests() {
        GenericLinkedList<Quest> cq = new GenericLinkedList<Quest>();
        int len = this.quests.length();
        for (int i = 0; i < len; i++) {
            Quest tmpq = this.quests.getItem(i);
            
            if (tmpq.itemCompleted(this.getInventory()) && !tmpq.isCompleted() && tmpq.isActive()) {
                cq.insert(tmpq);
            }
        }

        return cq;
    }

    /**
     * get active quests
     * @return active quets
     */
    public GenericLinkedList<Quest> getActiveQuests() {
        GenericLinkedList<Quest> aq = new GenericLinkedList<>();
        int len = this.quests.length();
        for (int i = 0; i < len; i++) {
            if (this.quests.getItem(i).isActive()) {
                aq.insert(this.quests.getItem(i));
            }
        }

        return aq;
    }

    /**
     * questlog to string -- useless?? XXX
     * @return questlog in stringformat
     */
    public String questlog() {
        return String.format("Quests:\n%s", this.getActiveQuests());
    }

    /**
     * Heilbrunnen (Kirche)
     */
    public void fountainHeal() {
        this.setHp(this.getMaxHp());
        setAtk(getRate());
        this.ap = this.maxAp;
    }
    
    /**
     * Items hinzufuegen
     */
    public void restoreItem() {
        this.remainingItemUses = this.maxItem;
    }
    
    
    /**
     * Schmied (Werkstatt)
     */
    public void smith() {
        setRate(getRate() * 1.05);
        setAtk(getRate());
    }

    /**
     * AP-Regeneration
     *
     * @param reg Integer-Wert, wieviel regeneriert werden soll
     * @return Integer-Wert, wieviel regenriert wurde
     */
    public int regenerateAp(int reg) {
        int d = this.ap;
        this.ap = this.ap + reg;
        if (this.ap > this.maxAp) {
            this.ap = this.maxAp;
        } 
        return (this.ap - d);
    } 

    /**
     * AP-Regeneration
     *
     * @return Integer-Wert, wieviel regenriert wurde
     */
    public int regenerateAp() {       
        int d = this.ap;        
        this.ap = this.ap + this.apRegen;        
        if (this.ap > this.maxAp) {            
            this.ap = this.maxAp;            
        } 
        return (this.ap - d);      
    } 
    
    /**
     * Darstellung der Spielwerte
     *
     * @return Statuswerte der Spielfigur
     */
    public String toString() {   
        return (this.getName() + " -- HP " + this.getHp() + " -- ATK " + this.getAtk() + " -- AP " + this.ap);
    }
    
    /**
     * main method -- testing only
     * @param args commandline args
     */
    public static void main(String[] args) {
        Player player = new Player();

        player.getQuests().getItem(1).activate();

        System.out.println(player.questlog());


        // adding roses
        Item itm = new Item();
        itm.setName("Blume");
        player.getInventory().insert(itm);
        player.getInventory().insert(itm);
        player.getInventory().insert(itm);
        player.getInventory().insert(itm);

        System.out.println(player.getInventory());
        
        System.out.println("completed:\n" + player.getCompletedQuests());
    }
}
