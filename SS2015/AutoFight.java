/**
 *  class handeling a gui for fights
 *  @author Philipp 4569839 Gruppe 5b
 *  @author Eva Vanessa Bolle 4528650 Gruppe 5b
 */
class AutoFight extends Thread {
    /** player */
    private Player player;
    /** emeny */
    private Monster enemy;

    /** FightClub */
    private FightClub fc;

    /** this needs to be false */
    public boolean stop;

    /**
     * Konstruktor
     * @param fc FightClub
     * @param player Player
     * @param enemy Monster
     */
    public AutoFight(FightClub fc, Player player, Monster enemy) {
        this.player = player;
        this.enemy = enemy;
        this.fc = fc;
        this.stop = false;
    }

    /**
     * the fight
     */
    public void run() {
        while (true) {
            try {
                Thread.sleep(1337); 
            } catch (InterruptedException e) { }

            if (stop || player.isDefeated() || enemy.isDefeated()) {
                break;
            }

            synchronized (player) {
                player.regenerateAp(10);

                synchronized (enemy) {
                    enemy.attack(player);
                }
            }
            this.fc.update();
        }
    }
}
