/**
 * Yet another AVL tree implementation done in Java
 * This one contains recursion to do its job
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 *
 * @param <T> java generic wildcard
 */
public class GenericAVLTree<T extends Secret<T>> implements List<T>, java.io.Serializable {

    /**
     * Das aktuelle T
     */
    private T item;

    /**
     * Der linke Teilbaum
     */
    private GenericAVLTree<T> left;

    /**
     *Der rechte Teilbaum
     */
    private GenericAVLTree<T> right;

    /**
     * Konstruktor, erzeugt leere Liste
     */
    public GenericAVLTree() {
        this.item = null;
        this.left = null;
        this.right = null;
    }

    /**
     * Ueberprueft ob der Baum leer ist
     *
     * @return true, Baum ist leer
     */
    public boolean isEmpty() {
        return this.left == null && this.right == null;
    }

    /**
     * Gibt die Anzahl der Elemente im Baum zurück
     *
     * @return die Groesse
     */
    public int length() {
        if (this.isEmpty()) {
            return 0;
        } else {
            return (this.left.length() + this.right.length() + 1);
        }
    }

    /**
     * Prueft ob ein T in der Liste ist
     *
     * @param x das T
     * @return true, x ist in der Liste enthalten
     */
    public boolean isInList(T x) {
        if (this.isEmpty()) {
            return false;
        } else if (this.item.equals(x)) {
            return true;
        } else {
            return (this.left.isInList(x) || this.right.isInList(x));
        }
    }

    /**
     * Gibt das erste (ganz linke) T des Baums zurueck
     *
     * @return das erste T
     * @throws IllegalStateException wenn die Liste leer ist
     */
    public T firstItem() throws IllegalStateException {
        //who the heck needs this?
        if (this.isEmpty()) {
            throw new IllegalStateException();
        } else {
            return this.getItem(0);
        }
    }

    /**
     * Gibt das i-te T der Liste zurueck
     *
     * @param i der Index
     * @return das i-te T
     * @throws IndexOutOfBoundsException wenn i < 0 oder  i >= length()
     */
    public T getItem(int i) throws IndexOutOfBoundsException {
        if (i < 0 || i >= this.length()) {
            throw new IndexOutOfBoundsException();
        } else if (i < this.left.length()) {
            return this.left.getItem(i);
        } else if (i > this.left.length()) {
            return this.right.getItem(i - this.left.length() - 1);
        } else {
            return this.item;
        }
    }

    /**
     * Checks if tree is balanced
     *
     * @return tree balanced?
     */
    private boolean isBalanced() {
        if (this.isEmpty()) {
            return true;
        } else {
            boolean b = ((this.left.height() - this.right.height()) * (this.left.height() - this.right.height())) < 2;
            return ((b && this.left.isBalanced()) && this.right.isBalanced());
        }
    }

    /**
     * Gets height of tree
     * 
     * @return height
     */
    public int height() {
        if (this.isEmpty()) {
            return 0;
        } else {
            int a = this.left.height();
            int b = this.right.height();
            return a < b ? b + 1 : a + 1;
        }
    }

    /**
     * Fuegt ein Element sortiert in den Baum ein
     *
     * @param x das Element
     * @return der geaenderte Baum
     */
    public GenericAVLTree<T> insert(T x) {
        if (x.equals(null)) {
            return this;
        }
        if (this.isEmpty()) {
            GenericAVLTree<T> tmpleft = new GenericAVLTree<T>();
            GenericAVLTree<T> tmpright = new GenericAVLTree<T>();
            this.left = tmpleft;
            this.right = tmpright;
            this.item = x;
            return this.balance();
        } else if (this.item.compareTo(x) <= 0) {
            this.right.insert(x);
            return this.right.balance();
        } else {
            this.left.insert(x);
            return this.left.balance();
        }
    }

    /**
     * balances the tree
     *
     * @return the balanced tree
     */
    public GenericAVLTree<T> balance() {
        if (this.isBalanced()) {
            return this;
        } else if (!this.left.isBalanced()) {
            this.left.balance();
            return this.balance();
        } else if (!this.right.isBalanced()) {
            this.right.balance();
            return this.balance();
        } else {
            GenericAVLTree<T> a = new GenericAVLTree<T>();
            GenericAVLTree<T> b = new GenericAVLTree<T>();
            GenericAVLTree<T> c = new GenericAVLTree<T>();
            a = this;
            if (this.left.height() < this.right.height()) {
                b = this.right;
            } else {
                b = this.left;
            }
            if (b.left.height() < b.right.height()) {
                c = b.right;
            } else {
                c = b.left;
            }

            T x, y, z;
            GenericAVLTree<T> d, e, f, tw, tx, ty, tz;

            //here's the break where things will be copied
            if (a.item.compareTo(b.item) < 0 && a.item.compareTo(c.item) < 0) { // a is leftmost subtree
                x = a.item.clone();
                if (b.item.compareTo(c.item) < 0) { //Order is a, b, c
                    y = b.item.clone();
                    z = c.item.clone();
                    d = a.clone();
                    e = b.clone();
                    f = c.clone();
                } else { //Order is a, c, b
                    y = c.item.clone();
                    z = b.item.clone();
                    d = a.clone();
                    e = c.clone();
                    f = b.clone();
                }
            } else if (b.item.compareTo(a.item) < 0 && b.item.compareTo(c.item) < 0) { // b is leftmost subtree
                x = b.item.clone();
                if (a.item.compareTo(c.item) < 0) { //Order is b, a, c
                    y = a.item.clone();
                    z = c.item.clone();
                    d = b.clone();
                    e = a.clone();
                    f = c.clone();
                } else { //Order is b, c, a
                    y = c.item.clone();
                    z = a.item.clone();
                    d = b.clone();
                    e = c.clone();
                    f = a.clone();
                }
            } else { // c is leftmost subtree
                x = c.item.clone();
                if (a.item.compareTo(b.item) < 0) { //Order is c, a, b
                    y = a.item.clone();
                    z = b.item.clone();
                    d = c.clone();
                    e = a.clone();
                    f = b.clone();
                } else { //Order is c, b, a
                    y = b.item.clone();
                    z = a.item.clone();
                    d = c.clone();
                    e = b.clone();
                    f = a.clone();
                }
            }
            //subtree copying
            if (d.height() < e.height() && e.height() < f.height()) { //case 1
                tw = d.left; 
                tx = d.right;
                ty = e.right;
                tz = f.right;
            } else if (f.height() < e.height() && e.height() < d.height()) { //case 2
                tw = d.left;
                tx = e.left;
                ty = f.left;
                tz = f.right;
            } else if (e.height() < d.height() && d.height() < f.height()) { //case 3
                tw = d.left;
                tx = e.left;
                ty = e.right;
                tz = f.right;
            } else { //case 4
                tw = d.left;
                tx = e.left;
                ty = e.right;
                tz = f.right;
            }
            //Copying comlpeted
            GenericAVLTree<T> nt = new GenericAVLTree<T>();
            //insertion is happening here
            nt.item = y;
            nt.left = new GenericAVLTree<T>();
            nt.right = new GenericAVLTree<T>();
            nt.left.item = x;
            nt.right.item = z;
            nt.left.left = tw;
            nt.left.right = tx;
            nt.right.left = ty;
            nt.right.right = tz;
            this.item = nt.item;
            this.left = nt.left;
            this.right = nt.right;
            return this.balance();
        }
    }
    

    /**
     * method for creating clones
     * @return cloned object
     */
    public GenericAVLTree<T> clone() {
        if (this.isEmpty()) {
            return new GenericAVLTree<T>();
        } else {
            GenericAVLTree<T> nt = new GenericAVLTree<T>();
            nt.item = this.item.clone();
            nt.left = this.left.clone();
            nt.right = this.right.clone();
            return nt;
        }
    }

    /**
     * Haengt ein Element unsortiert an das Ende des Baumes an
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericAVLTree<T> append(T x) {
        throw new UnsupportedOperationException("AVL-Trees are always sorted, thus appending makes no sense");
    }

    /**
     * Loescht das erste vorkommen des Ts x
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericAVLTree<T> delete(T x) {
        if (!this.isInList(x)) {
            return this;
        } else if (this.item.equals(x)) {
            this.delete();
            return this.balance();
        } else if (this.item.compareTo(x) <= 0) {
            this.right.delete(x);
            return this.balance();
        } else {
            this.left.delete(x);
            return this.balance();
        }
    }

    /**
     * Loescht die Wurzel des Baums
     *
     * @return die geanderte Liste
     */
    public GenericAVLTree<T> delete() {
        if (this.isEmpty()) {
            return this;
        } else if (this.left.isEmpty()) {
            this.item = this.right.item;
            this.left = this.right.left;
            this.right = this.right.right;
            return this;
        } else if (this.right.isEmpty()) {
            this.item = this.left.item;
            this.right = this.left.right;
            this.left = this.left.left;
            return this;
        } else {
            T x = this.right.getItem(0).clone(); 
            this.right.delete(this.right.getItem(0));
            this.item = x;
            return this;
        }
    }

    /**
     * input int to index int
     * @param i Input number
     * @return Index
     */
    public int inputToIndex(int i) {
        int j = 0;
        int k = 0;
        for (k = 0; k < this.length() && j < i; k++) {
            if (!(k + 1 < this.length() && this.getItem(k).equals(this.getItem(k + 1)))) {
                j++;
            }
        }
        return k;
    }

    /**
     *macht aus der Liste nen kurzen String
     *(gezählte Elemente)
     *
     *@return String
     */
    public String toShortString() {
        String s = "";
        int k = 0;
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            int j = 1; 
            for (int i = 0; i < this.length(); i++) {
                if (i + 1 < this.length() && this.getItem(i).equals(this.getItem(i + 1))) {
                    j++;
                } else {
                    s = s + k + " - " + this.getItem(i).toString() + " (" + j + "x)\n";
                    j = 1;
                    k++;
                }
            }
        }
        return s;
    }


    /**
     * yet another toString method - recursively lists tree structure
     * @param str string to start with
     * @return treestruct
     */
    public String toString2(String str) {
    
        String newstr = str + " -> " + this.item + "\n";
        
        if (this.left != null) {
            newstr += this.left.toString2(str + "l");
        }

        if (this.right != null) {
            newstr += this.right.toString2(str + "r");
        }

        return newstr;
    }

    /**
     *macht aus der Liste nen String
     *
     *@return String
     */
    public String toString() {
        String s = "";
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            for (int i = 0; i < this.length(); i++) {
                s = s + i + " - " + this.getItem(i).toString() + "\n";
            }
        }
        return s;
    }


    /**
     * main method for testing purposes
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        GenericAVLTree<Quest> l = new GenericAVLTree<Quest>();

        int[] a = {6, 5, 4, 3, 2, 1, 0};
        int citm = a.length;
        Quest[] itm = new Quest[citm];
        
        System.out.println("Items to add...");
        for (int i = 0; i < citm; i++) {
            itm[i] = new Quest("Quest " + a[i], "Prequest " + i, "Item " + i, i + 2);
            //itm[i].setName("Item No. " + ((int) (Math.random() * 100)) );
            System.out.println(i + ". " + itm[i]);
        }
        
        
        System.out.println("\nList stuff...");

        for (int i = 0; i < citm; i++) {
            l.insert(itm[i]);
            System.out.println(l.isBalanced());
            System.out.println("length: " + l.length());
            System.out.println(l);
        }


        System.out.println("== GET FIRST ITEM");
        System.out.println(l.firstItem());


        System.out.println("== GET ITEM BY INDEX");
        for (int i = 0; i < citm; i++) {
            System.out.println("getItem(" + i + ") -> " + l.getItem(i));
        }

        System.out.println(l.toString2(""));
        System.out.println(l.isBalanced());

        System.out.println("== DELETE FIRST");
        l.delete();
        System.out.println("deleted");
        System.out.println(l.isBalanced());
        System.out.println(l);

        System.out.println(l.toString2(""));

        System.out.println("== CHECK IF IN LIST");
        for (int i = 0; i < citm; i++) {
            if (l.isInList(itm[i])) {
                System.out.println("  found   \"" + itm[i] + "\"");
            } else {
                System.out.println("not found \"" + itm[i] + "\"");
            }
        }

        System.out.println("== DELETE ALL");
        for (int i = 0; i < citm; i++) {
            System.out.println("delete(" + itm[i] + ")");
            l.delete(itm[i]);
            System.out.println(l.isBalanced());
            System.out.println(l);
        }

        System.out.println("== END OF TEST");
    }
}
