import java.util.Scanner;
/**
 *  Class for handeling Shops buying and selling
 * 
 * @author Eva 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 */

public class Shop {

    /** inventory of the trader */
    private GenericLinkedList<Item> inventory;

    /** Constructor */
    public Shop() {
        //System.out.println("Generating LinkedList...");
        inventory = new GenericLinkedList<Item>();
        //int r = (int)(Math.random() * 15);
        int r = 10;
        for (int i = 0; i < r; i++) {
            //System.out.println("Generating Item " + i + "...");
            Item m = new Item();
            //System.out.println(m);
            //m.setValue((int) (m.getValue() * 1.1));
            //System.out.println("Inserting Item...");
            this.inventory.insert(m);
            //System.out.println("Item inserted...");
        }
        //System.out.println("All Items inserted...");
    }

    /**
     * Manages usage of the shop
     * @param p the player
     */
    public void use(Player p) {
        System.out.println("Shop:\n" + offer() + "\n"
            + "Spieler:\n" + p.getInventory().toShortString() 
            + "Geld: " + p.getGold() + "EUR\n\n"
            + "1 - Kaufen\n2 - Verkaufen\n3 - Verlassen");
        Scanner sc = new Scanner(System.in);
        String in = sc.nextLine();
        int i;
        switch (in) {
            case "1":
                //Kaufen
                System.out.println("Welches Item?");
                //i = sc.nextLine();
                try {
                    i = sc.nextInt();
                    this.buy(p, i);
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Tut mir Leid, das verkaufen wir nicht!");
                }
                this.use(p);
                break;

            case "2":
                //verkaufen
                System.out.println("Welches Item?");
                //i = sc.nextLine();
                try {
                    i = sc.nextInt();
                    this.sell(p, i);
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Das hast du nicht!");
                }
                this.use(p);
                break;

            case "3":
                //verlassen
                System.out.println("Vielen Dank und bis bald!");
                break;

            default:
                System.out.println("Bitte nochmal versuchen:");
                this.use(p);
                break;
        }
    }
    /**
     *Output
     *
     *@return String
     */
    public String toString() {
        return this.inventory.toShortString();
    }

    /**
     *Player buys item
     *
     * @param p Player
     * @param i Index
     */ 
    public void buy(Player p, int i) {
        Item m = this.inventory.getItem(this.inventory.inputToIndex(i));
        if (p.getGold() >= (int) (m.getValue() * 1.1 + 1)) {
            GenericLinkedList<Item> pl = p.getInventory();
            this.inventory.delete(m);
            p.setGold(p.getGold() - (int) (m.getValue() * 1.1 + 1));
            //Wert von m verringern
            //m.setValue((int) (m.getValue() * 0.9 - 1));
            pl.insert(m);
            p.setInventory(pl);
        } else {
            System.out.println("Dein Geld reicht leider nicht aus!");
        }
    }

    /**
     *Player sells item
     *
     *@param p Player
     *@param i Index
     */
    public void sell(Player p, int i) {
        Item m = p.getInventory().getItem(p.getInventory().inputToIndex(i));
        GenericLinkedList<Item> pl = p.getInventory();
        pl.delete(m);
        p.setInventory(pl);
        p.setGold(p.getGold() + m.getValue());
        //Wert von m erhöhen
        //m.setValue((int) (m.getValue() * 1.1 + 1));
        this.inventory.insert(m);
    }

    /**
     *  creation of shopping-list
     *  @return the printable list
     */
    private String offer() {
        String s = "";
        int k = 0;
        if (this.inventory.isEmpty()) {
            s = "-empty-\n";
        } else {
            int j = 1;  //See comment in GenericLinkedList.java - toString()
            for (int i = 0; i < this.inventory.length(); i++) {
                if (i + 1 < this.inventory.length() 
                        && this.inventory.getItem(i).equals(
                            this.inventory.getItem(i + 1))) {
                    j++;
                } else {
                    s = s + k + " - " + this.inventory.getItem(i).toShopString() + " (" + j + "x)\n";
                    j = 1;
                    k++;
                } 
            }
        }
        return s;
    }

    /**
     * Main-method for testing
     * 
     * @param args commandline argument
     */
    public static void main(String[] args) {
        Player p = new Player();
        GenericLinkedList<Item> pl = new GenericLinkedList<Item>();
        for (int i = 0; i < 10; i++) {
            pl.insert(new Item());
        }
        p.setInventory(pl);
        Shop s = new Shop();
        s.use(p);
    } 
}
