/**
 * Yet another AVL tree implementation done in Java
 * This one contains recursion to do its job
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 *
 * @param <T> java generic wildcard
 */
public class GenericBinaryTree<T extends Secret<T>> implements List<T> {

    /**
     * Das aktuelle T
     */
    private T item;

    /**
     * Der linke Teilbaum
     */
    private GenericBinaryTree<T> left;

    /**
     *Der rechte Teilbaum
     */
    private GenericBinaryTree<T> right;

    /**
     * Konstruktor, erzeugt leere Liste
     */
    public GenericBinaryTree() {
        this.item = null;
        this.left = null;
        this.right = null;
    }

    /**
     * Ueberprueft ob der Baum leer ist
     *
     * @return true, Baum ist leer
     */
    public boolean isEmpty() {
        return this.left == null && this.right == null;
    }

    /**
     * Gibt die Anzahl der Elemente im Baum zurück
     *
     * @return die Groesse
     */
    public int length() {
        if (this.isEmpty()) {
            return 0;
        } else {
            return (this.left.length() + this.right.length() + 1);
        }
    }

    /**
     * Prueft ob ein T in der Liste ist
     *
     * @param x das T
     * @return true, x ist in der Liste enthalten
     */
    public boolean isInList(T x) {
        if (this.isEmpty()) {
            return false;
        } else if (this.item.equals(x)) {
            return true;
        } else {
            return (this.left.isInList(x) || this.right.isInList(x));
        }
    }

    /**
     * Gibt das erste (ganz linke) T des Baums zurueck
     *
     * @return das erste T
     * @throws IllegalStateException wenn die Liste leer ist
     */
    public T firstItem() throws IllegalStateException {
        //who the heck needs this?
        if (this.isEmpty()) {
            throw new IllegalStateException();
        } else {
            return this.getItem(0);
        }
    }

    /**
     * Gibt das i-te T der Liste zurueck
     *
     * @param i der Index
     * @return das i-te T
     * @throws IndexOutOfBoundsException wenn i < 0 oder  i >= length()
     */
    public T getItem(int i) throws IndexOutOfBoundsException {
        if (i < 0 || i >= this.length()) {
            throw new IndexOutOfBoundsException();
        } else if (i < this.left.length()) {
            return this.left.getItem(i);
        } else if (i > this.left.length()) {
            return this.right.getItem(i - this.left.length() - 1);
        } else {
            return this.item;
        }
    }

    /**
     * Checks if tree is balanced
     *
     * @return tree balanced?
     */
    private boolean isBalanced() {
        return true;
    }

    /**
     * Gets height of tree
     * 
     * @return height
     */
    public int height() {
        if (this.isEmpty()) {
            return 0;
        } else {
            int a = this.left.height();
            int b = this.right.height();
            return a < b ? b + 1 : a + 1;
        }
    }

    /**
     * Fuegt ein Element sortiert in den Baum ein
     *
     * @param x das Element
     * @return der geaenderte Baum
     */
    public GenericBinaryTree<T> insert(T x) {
        if (x.equals(null)) {
            return this;
        }
        if (this.isEmpty()) {
            GenericBinaryTree<T> tmpleft = new GenericBinaryTree<T>();
            GenericBinaryTree<T> tmpright = new GenericBinaryTree<T>();
            this.left = tmpleft;
            this.right = tmpright;
            this.item = x;
            return this;
        } else if (this.item.compareTo(x) <= 0) {
            return this.right.insert(x);
        } else {
            return this.left.insert(x);
        }
    }

    /**
     * balances the tree
     *
     * @return the balanced tree
     */
    public GenericBinaryTree<T> balance() {
        return this;
    }

    /**
     * Haengt ein Element unsortiert an das Ende des Baumes an
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericBinaryTree<T> append(T x) {
        throw new UnsupportedOperationException("AVL-Trees are always sorted, thus appending makes no sense");
    }

    /**
     * Loescht das erste vorkommen des Ts x
     *
     * @param x das T
     * @return die geanderte Liste
     */
    public GenericBinaryTree<T> delete(T x) {
        if (!this.isInList(x)) {
            return this;
        } else if (this.item.equals(x)) {
            return this.delete();
        } else if (this.item.compareTo(x) <= 0) {
            return this.right.delete(x);
        } else {
            return this.left.delete(x);
        }
    }

    /**
     * Loescht die Wurzel des Baums
     *
     * @return die geanderte Liste
     */
    public GenericBinaryTree<T> delete() {
        if (this.isEmpty()) {
            return this;
        } else if (this.left.isEmpty()) {
            this.item = this.right.item;
            this.left = this.right.left;
            this.right = this.right.right;
            return this;
        } else if (this.right.isEmpty()) {
            this.item = this.left.item;
            this.right = this.left.right;
            this.left = this.left.left;
            return this;
        } else {
            T x = this.right.getItem(0).clone(); 
            this.right.delete(this.right.getItem(0));
            this.item = x;
            return this;
        }
    }

    /**
     * input int to index int
     * @param i  is not even used here
     * @return Index
     */
    public int inputToIndex(int i) {
        throw new UnsupportedOperationException("AVL-Trees are always sorted, thus defining the place"
            + "at which an element should be inserted makes no sense");
    }

    /**
     *macht aus der Liste nen kurzen String
     *(gezählte Elemente)
     *
     *@return String
     */
    public String toShortString() {
        String s = "";
        int k = 0;
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            int j = 1; 
            for (int i = 0; i < this.length(); i++) {
                if (i + 1 < this.length() && this.getItem(i).equals(this.getItem(i + 1))) {
                    j++;
                } else {
                    s = s + k + " - " + this.getItem(i).toString() + " (" + j + "x)\n";
                    j = 1;
                    k++;
                }
            }
        }
        return s;
    };


    /**
     * recursive toString method
     * @param str initial string
     * @return treestructure represented as a string
     */
    public String toString2(String str) {
    
        String newstr = str + " -> " + this.item + "\n";
        
        if (this.left != null) {
            newstr += this.left.toString2(str + "l");
        }

        if (this.right != null) {
            newstr += this.right.toString2(str + "r");
        }

        return newstr;
    }

    /**
     *macht aus der Liste nen String
     *
     *@return String
     */
    public String toString() {
        String s = "";
        if (this.isEmpty()) {
            s = "-empty- \n";
        } else {
            for (int i = 0; i < this.length(); i++) {
                s = s + i + " - " + this.getItem(i).toString() + "\n";
            }
        }
        return s;
    }


    /**
     * main method for testing purposes
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        GenericBinaryTree<Quest> l = new GenericBinaryTree<Quest>();

        int[] a = {3, 2, 0, 1, 5, 4, 6};
        int citm = a.length;
        Quest[] itm = new Quest[citm];
        
        System.out.println("Empty: (true) " + l.isEmpty());
        System.out.println("Height: (0) " + l.height());
        System.out.println("Length: (0) " + l.length());

        System.out.println("Items to add...");
        for (int i = 0; i < citm; i++) {
            itm[i] = new Quest("Quest " + a[i], "Prequest " + i, "Item " + i, i + 2);
            itm[i].setName("Item No. " + ((int) (Math.random() * 100)) );
            System.out.println(i + ". " + itm[i]);
        }
        
        
        System.out.println("\nList stuff...");

        for (int i = 0; i < citm; i++) {
            l.insert(itm[i]);
            System.out.println("length: " + l.length());
            System.out.println(l);
        }


        System.out.println("== GET FIRST ITEM");
        System.out.println(l.firstItem());


        System.out.println("== GET ITEM BY INDEX");
        for (int i = 0; i < citm; i++) {
            System.out.println("getItem(" + i + ") -> " + l.getItem(i));
        }

        System.out.println(l.toString2(""));

        System.out.println("== DELETE FIRST");
        l.delete();
        System.out.println("deleted");
        System.out.println(l);

        System.out.println(l.toString2(""));

        System.out.println("== CHECK IF IN LIST");
        for (int i = 0; i < citm; i++) {
            if (l.isInList(itm[i])) {
                System.out.println("  found   \"" + itm[i] + "\"");
            } else {
                System.out.println("not found \"" + itm[i] + "\"");
            }
        }

        System.out.println("== DELETE ALL");
        for (int i = 0; i < citm; i++) {
            System.out.println("delete(" + itm[i] + ")");
            l.delete(itm[i]);
            System.out.println("deleted");
            System.out.println(l);
        }

        System.out.println("== END OF TEST");
    }
}
