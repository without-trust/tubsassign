import java.io.*;
/**
 *  SaveGameManager - manages game saving
 *
 *  @author Eva Vanessa Bolle 4528650 Gruppe 5b
 *  @author Philipp 4569839 Gruppe 5b
 */
class SaveGameManager {
    
    /**
     * saving an object to a file
     * @param x object to be saved
     * @param filename data will be written to this file
     * @param <T> Template
     * @throws IOException IOException
     */
    public static <T> void saveObject(T x, String filename) throws IOException {

        System.out.println("saving, please do not turn off the device.");

        FileOutputStream fs = new FileOutputStream(filename);
        ObjectOutputStream os = new ObjectOutputStream(fs);
        os.writeObject(x);
        os.close();
        fs.close();
        
        System.out.println("written to file: " + filename);
    }

    /**
     * loading an object from file
     * @param filename specifies file to load from
     * @param <T> Template
     * @return the object that was loaded
     * @throws IOException IOException
     */
    @SuppressWarnings("unchecked")
    public static <T> T loadObject(String filename) throws IOException {
        T x = null;
        try {
            FileInputStream fs = new FileInputStream(filename);
            ObjectInputStream os = new ObjectInputStream(fs);
            x = (T) os.readObject();
        } catch (ClassNotFoundException e) {
            System.out.println("error parsing file: class not found" + e);
        } 

        return x;
    }

    /**
     * main method - for testing only
     * @param args commandline args
     */
    public static void main(String[] args) {
        Item itm = new Item("cookies", 0x1337, 0x42);
        System.out.println("to be saved: " + itm);
        try {
            SaveGameManager.saveObject(itm, "savegamemanagertest.save");

            Item itm2 = SaveGameManager.loadObject("savegamemanagertest.save");
            System.out.println("loaded this: " + itm2);
        } catch (IOException e) {
            System.out.println("IO ERROR: " + e);
        }
    }
}

