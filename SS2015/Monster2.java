/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet die Monster2-Methode (Vampir).
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 2a
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Monster2 extends Monster {
    
    /**
     * Konstruktor
     * @param maxHp Die maximale Anzahl an Lebenspunkten
     * @param hitChance Die Trefferwahrscheinlichkeit
     */
    public Monster2(int maxHp, double hitChance) {
        this.setMaxHp(maxHp);
        this.setHp(maxHp);
        this.setHitChance(hitChance);
        this.setName("Vampir");
        setAtk(getRate());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            this.getInventory().insert(new Item());
        }
        this.setGold((int) (Math.random() * 133.7));
    }
    
    /**
     * Konstruktor (Standardwerte fuer das Monster) 
     */
    public Monster2() {
        this.setMaxHp(125);
        this.setHp(this.getMaxHp());
        this.setHitChance(0.5);
        this.setName("Vampir");
        setAtk(getRate());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            this.getInventory().insert(new Item());
        }
        this.setGold((int) (Math.random() * 133.7));
    }
    
    /**
     * Angriff des Monsters auf den Spieler 
     * @param player Angegriffener Spieler
     * @return Integer d, der bei Teffer den Schaden angibt, sonst -1    
     */
    public int attack(Player player) {
        double r = Math.random();
        int d;
        System.out.println("Monster greift an!");
        if (r < this.getHitChance()) {
            d = (int) ((0.0 + this.getAtk()) * (Math.random() + 1));
            player.takeDamage(d);
            this.setHp(this.getHp() + (int) (0.5 * d));
            if (this.getHp() > this.getMaxHp()) {
                this.setHp(this.getMaxHp());
            }
            setAtk(getRate());
            System.out.println("Du wurdest getroffen!");
        } else {
            d = -1;
            System.out.println("Monster hat dich verfehlt!");
        }
        return d;
    }
    
}
