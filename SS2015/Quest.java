import java.io.Serializable;
/**
 * Simple class for quests
 * @author Eva 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 */
public class Quest implements Secret<Quest>, Serializable {

    /** The Name */
    private String name;

    /** The Name of the prequest */
    private String prequest;

    /** if quest was completed */
    private boolean completed;

    /** if quest is activated */
    private boolean active;

    /** The Item required */
    private String item;

    /** how many of the items are required */
    private int quantity;

    /** 
     * Instantiates a new Quest
     */
    public Quest() {
        String[] names = {
            "Mission #1",
            "The Quest",
            "the thing you are supposed to do",
            "get some money for the greedy king",
            "protect freedom of speech",
            "Mission Impossible"};
        String[] prequests = names;
        String[] items = {
            "Permanentmarker",
            "Battery",
            "calculator",
            "aimbot.exe",
            "Item 2121"};
        
        this.name = names[(int) (Math.random() * (double) names.length)];
        this.prequest = prequests[(int) (Math.random() * (double) prequests.length)];
        this.item = items[(int) (Math.random() * (double) items.length)];
        this.quantity = (int) (Math.random() * 11.11);
        this.completed = false;
        this.active = false;
    }

    /**
     * Instantiates a new Quest
     *
     * @param name  the name
     * @param prequest the prequest
     * @param item the required item
     * @param quantity the quantity
     */
    public Quest(String name, String prequest, String item, int quantity) {
        this.name = name;
        this.completed = false;
        this.active = false;
        this.prequest = prequest;
        this.item = item;
        this.quantity = quantity;
    }

    /**
     * method to clone object
     * @return cloned object
     */
    public Quest clone() {
        Quest nq = new Quest(this.getName(), this.getPrequest(), this.getItem(), this.getQuantity());
        if (this.isActive()) {
            nq.activate();
        }
        if (this.isCompleted()) {
            nq.complete();
        }
        return nq;
    }
    
    /** @return the name */
    public String getName() { return this.name; }
    /** @param name the name */
    public void setName(String name) { this.name = name; }

    /** @return if quest is completed */
    public boolean isCompleted() { return this.completed; }
    /** complete Quest */
    public void complete() { this.completed = true; }

    /** @return if quest is activated */
    public boolean isActive() { return this.active; }
    /** activate quest */
    public void activate() { this.active = true; }

    /** @return the prequest */
    public String getPrequest() { return this.prequest; }
    /** @param prequest the prequest */
    public void setPrequest(String prequest) { this.prequest = prequest; }

    /** @return the item */
    public String getItem() { return this.item; }
    /** @param item the item */
    public void setItem(String item) { this.item = item; }

    /** @return the quantity */
    public int getQuantity() { return this.quantity; }
    /** @param quantity the quantity */
    public void setQuantity(int quantity) { this.quantity = quantity; }

    /**
     * check for equality
     *
     * @param obj the object to compare to
     * @return equality
     */
    public boolean equals(Object obj) {
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        if (!((Quest) obj).name.equals(this.name)) {
            return false;
        }

        if (!((Quest) obj).prequest.equals(this.prequest)) {
            return false;
        }

        if (!((Quest) obj).item.equals(this.item)) {
            return false;
        }

        if (((Quest) obj).quantity != this.quantity) {
            return false;
        }

        // if none of these checks differs, they are probably equal
        return true;
    }

    /**
     * checking wether or not the quest was completed
     * @param inventory the players inventory
     * @return wether or not the quest was completed
     */
    public boolean itemCompleted(GenericLinkedList<Item> inventory) {
        int itmcount = 0;

        int len = inventory.length();
        for (int i = 0; i < len; i++) {
            if (inventory.getItem(i).getName().equals(this.getItem())) {
                itmcount++;
            }
        }

        return itmcount >= this.getQuantity();
    }

    /**
     * Stringify instance
     * @return "string" of instane
     */
    public String toString() {
        return String.format("Quest: %-20s - Prequest: %-20s - %2dx %-20s - %s" , 
                this.name, 
                this.prequest.equals("") ? "-keine-" : this.prequest,
                this.quantity,
                this.item,
                this.isCompleted() ? "abgeschlossen" : "ausstehend");
    }
    
    /** 
     * compareTo method to compare two Items 
     *
     * @param quest the Object to compare to
     * @return how to sort these to Items
     */
    public int compareTo(Quest quest) {
        final int EQUAL = 0;

        if (this == quest) { 
            return 0;
        }

        if (this.equals(quest)) {
            return 0;
        }

        if (this.name.compareTo(quest.name) != 0) {
            return this.name.compareTo(quest.name);
        }

        if (this.prequest.compareTo(quest.prequest) != 0) {
            return this.prequest.compareTo(quest.prequest);
        }

        if (this.item.compareTo(quest.item) != 0) {
            return this.item.compareTo(quest.item);
        }

        if (this.quantity != quest.quantity) {
            return this.quantity - quest.quantity;
        }

        return 0;
    }

    /**
     * main method, mainly for testing
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        Quest q1 = new Quest("name1", "other1", "item1", 10);
        Quest q2 = new Quest("name2", "other2", "item2", 11);
        System.out.println(q1);
        System.out.println(q2);

        System.out.println("cmp q1, q2 : " + q1.compareTo(q2));
    }
}

