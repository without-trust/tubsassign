import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *  class handeling a gui for fights
 *  @author Philipp 4569839 Gruppe 5b
 *  @author Eva Vanessa Bolle 4528650 Gruppe 5b
 */
class FightClub extends JFrame implements ActionListener, WindowListener {

    /** player */
    private Player player;
    /** enemy */
    private Monster enemy;

    /** autofight */
    private AutoFight af;

    /**textarea used for logging*/
    private JTextArea txt;
    /**scrollable textarea generated from txt*/
    private JScrollPane txtscr;
    /**stats of player (no longer needed)*/
    //private JLabel stats;
    /**picture Area*/
    private JLabel pic;
    /**pictures*/
    private ImageIcon winning, losing;
    /**buttons */
    private JButton btnatk1, btnatk2, btnatk3, btnatk4, btnatk5;

    /** 
     * Constructor
     * used to create and initialise fightgui
     * @param player Player
     * @param enemy Enemy
     */
    FightClub(Player player, Monster enemy) {
        
        this.player = player;
        this.enemy = enemy;

        this.af = new AutoFight(this, player, enemy);
        this.af.start();

        setLayout(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        txt = new JTextArea(player + "\n" + enemy + "\n", 10, 80);
        txtscr = new JScrollPane(txt, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        //stats = new JLabel("TODO");
        //updateStats();
        winning = new ImageIcon("pics/winning.gif");
        losing = new ImageIcon("pics/losing.gif");
        pic = new JLabel(winning);
        btnatk1 = new JButton("Schwertangriff (20 Ap)");
        btnatk2 = new JButton("Trank (" + player.getRemainingItemUses() + ")");
        btnatk3 = new JButton("Ausruhen und Kraft tanken (40 Ap)");
        btnatk4 = new JButton("Revolver mit Silberkugeln (50 Ap)");
        btnatk5 = new JButton("Weihwasser (30 Ap)");

        txtscr.setBounds(23, 505, 600, 150);
        //stats.setBounds(23, 350, 600, 30);
        pic.setBounds(23, 5, 600, 300);
        btnatk1.setBounds(23, 400, 183, 30);
        btnatk2.setBounds(231, 400, 183, 30);
        btnatk5.setBounds(439, 400, 184, 30);
        btnatk3.setBounds(23, 445, 287, 30);
        btnatk4.setBounds(335, 445, 288, 30);

        add(txtscr);
        //add(stats);
        add(btnatk1);
        add(btnatk2);
        add(btnatk3);
        add(btnatk4);
        add(btnatk5);
        add(pic);

        addWindowListener(this);
        btnatk1.addActionListener(this);
        btnatk2.addActionListener(this);
        btnatk3.addActionListener(this);
        btnatk4.addActionListener(this);
        btnatk5.addActionListener(this);

        txt.setEditable(false);

        //setBackground(Color.white);
        setSize(650, 700);
        //setLocation(500,350);
        setLocationRelativeTo(null);
        setVisible(true);
        update();
    }

    /** @param event WindowEvent */
    public void windowOpened(WindowEvent event) { }
    /** @param event WindowEvent */
    public void windowOpening(WindowEvent event) { }
    /** @param event WindowEvent */
    public void windowClosing(WindowEvent event) {
        System.out.println("Closing");
        if (enemy.isDefeated()) {
            Sync.battleFinished();
            af.stop = true;
            dispose();
        } else {
            System.out.println("Monster is still alive");
        }
    }
    /** @param event WindowEvent */
    public void windowClosed(WindowEvent event) {
        Sync.battleFinished();
        af.stop = true;
        System.out.println("Closed");
    }
    /** @param event WindowEvent */
    public void windowIconified(WindowEvent event) { }
    /** @param event WindowEvent */
    public void windowDeiconified(WindowEvent event) { }
    /** @param event WindowEvent */
    public void windowActivated(WindowEvent event) { }
    /** @param event WindowEvent */
    public void windowDeactivated(WindowEvent event) { }

    /**
     * listener for button presses etc
     * @param event ActionEvent
     */
    public void actionPerformed(ActionEvent event) {
        int dmg, aktion;
        //System.out.println("actionPerformed: " + event.getSource());

        if (event.getSource() == btnatk1) { 
            aktion = 1; 
        } else if (event.getSource() == btnatk2) { 
            aktion = 2; 
        } else if (event.getSource() == btnatk3) { 
            aktion = 3; 
        } else if (event.getSource() == btnatk4) { 
            aktion = 4; 
        } else if (event.getSource() == btnatk5) { 
            aktion = 5; 
        } else { 
            aktion = 0;
        }

        //println("players turn:");
        switch (aktion) {
            case 1:
                //println("Du greifst das Monster an.");
                dmg = player.attack(enemy);
                if (dmg == -1) {
                    println("Du hast das Monster verfehlt!");
                } else {
                    println("Du hast dem Monster " + dmg + " Schaden zugefügt!");
                }
                break;

            case 2:
                boolean h = player.heal();
                if (h) {
                    println("Trank benutzt!");
                } else {
                    println("Keine weiteren Tränke!");
                }
                btnatk2.setText("Trank (" + player.getRemainingItemUses() + ")");
                break;

            case 3:
                dmg = player.specialattack1();
                if (dmg == -1) {
                    println("Du findest keine Ruhe!");
                } else {
                    println("Du fühlst dich etwas frischer. (+" + dmg + " HP)");
                }
                break;

            case 4:
                dmg = player.specialattack2(enemy);
                if (dmg == -1) {
                    println("Silberkugeln sind teuer, aber du feuerst trotzdem daneben.");
                } else {
                    println("Bam, Silberkugel voll in die Fresse. (-" + dmg + " HP)");
                }
                break;

            case 5:
                dmg = player.specialattack3(enemy);
                if (dmg == -1) {
                    println("'Weihwasser' fehlgeschlagen!");
                } else {
                    println("Weihwasser hat " + dmg + " Schaden verursacht!");
                }
                break;

            default:
                System.out.println("Ungueltige Aktion!");
                break;
        }

        //player.regenerateAp();
        //update();
    
    /*
        if (!enemy.isDefeated()) {
            //println("Das Monster greift an.");
            dmg = enemy.attack(player);
            if (dmg == -1) {
                println("Monster hat dich verfehlt!");
                //pic.setIcon(new ImageIcon("pics/wait.jpg"));
            } else {
                println("Das Monster fügt dir " + dmg + " Schaden zu.");
                //pic.setIcon(new ImageIcon("pics/waiting2.jpg"));
            }
        }
    */

        /*if ((1.0 * player.getHp() / player.getMaxHp()) >= (1.0 * enemy.getHp() / enemy.getMaxHp())) {
            pic.setIcon(new ImageIcon("pics/winning.gif"));
        } else {
            pic.setIcon(new ImageIcon("pics/losing.gif"));
        }*/

    //checkDeath();

        //updateStats();
        update();
    }

    /**
     * action if someone died
     */
    public void checkDeath() {
        if (player.isDefeated()) {
            System.out.println("Du wurdest getötet. YOU LOSE!");
            dispose();
            System.exit(1);
        } else if (enemy.isDefeated()) {
            println("Das Monster ist tot!");
            System.out.println("Das Monster ist tot!");

            Item tmpitm;

            while (!enemy.getInventory().isEmpty()) {
                tmpitm = enemy.getInventory().firstItem();
                player.getInventory().insert(tmpitm);
                enemy.getInventory().delete(tmpitm);
            }

            player.setGold(player.getGold() + enemy.getGold());
            player.regenerateAp(player.getMaxAp());

            dispose();
        }
    }

    /** updating stats */
    /*public void updateStats() {
        stats.setText("Spieler: " + player.getHp() + " HP - " 
            + player.getAp() + " AP - "
            + "Gegner: " + enemy.getHp() + " HP");
    }*/

    /**
     * write log to textarea
     * @param str string to write to textarea
     */
    public void println(String str) {
        txt.setText(txt.getText() + "\n" + str);
    }

    /**
     * enable/disable buttons, repaint
     */
    public void update() {
        if (player.getRemainingItemUses() == 0) {
            btnatk2.setEnabled(false);
        }
        if (player.getAp() < 20) {
            btnatk1.setEnabled(false);
            btnatk3.setEnabled(false);
            btnatk4.setEnabled(false);
            btnatk5.setEnabled(false);
        } else if (player.getAp() < 30) {
            btnatk1.setEnabled(true);
            btnatk3.setEnabled(false);
            btnatk4.setEnabled(false);
            btnatk5.setEnabled(false);
        } else if (player.getAp() < 40) {
            btnatk1.setEnabled(true);
            btnatk3.setEnabled(false);
            btnatk4.setEnabled(false);
            btnatk5.setEnabled(true);
        } else if (player.getAp() < 50) {
            btnatk1.setEnabled(true);
            btnatk3.setEnabled(true);
            btnatk4.setEnabled(false);
            btnatk5.setEnabled(true);
        } else {
            btnatk1.setEnabled(true);
            btnatk3.setEnabled(true);
            btnatk4.setEnabled(true);
            btnatk5.setEnabled(true);
        }
        if ((1.0 * player.getHp() / player.getMaxHp()) >= (1.0 * enemy.getHp() / enemy.getMaxHp())) {
            pic.setIcon(winning);
        } else {
            pic.setIcon(losing);
        }
        repaint();
        checkDeath();
    }

    /**
     * paint health bar
     * @param g Graphics
     */
    public void paint(Graphics g) {
        super.paint(g);
        
        //g.clearRect(0, 0, 650, 1000);
        //g.clearRect(25, 350, 292, 15);
        //g.clearRect(333, 350, 292, 15);
        //g.clearRect(25, 370, 292, 15);

        g.setColor(Color.white);
        g.fillRoundRect(25, 350, 292, 15, 7, 7);
        g.fillRoundRect(333, 350, 292, 15, 7, 7);
        g.fillRoundRect(25, 385, 292, 15, 7, 7);
        
        g.setColor(Color.red);

        int widtha = (int) (((1.0 * player.getHp()) / player.getMaxHp()) * 292);
        g.fillRoundRect(25, 350, widtha, 15, 7, 7);
        //System.out.println(widtha);
        g.setColor(Color.blue);
        int widthb = (int) (((1.0 * player.getAp()) / player.getMaxAp()) * 292);
        g.fillRoundRect(333, 350, widthb, 15, 7, 7);
        //System.out.println(widthb);
        g.setColor(Color.red);
        int widthc = (int) (((1.0 * enemy.getHp()) / enemy.getMaxHp()) * 292);
        g.fillRoundRect(25, 385, widthc, 15, 7, 7);
        //System.out.println(widthc);
        
        g.setColor(Color.black);
        int a = (int) (292.0 / (player.getMaxAp() / 10) + 0.5);
        for (int i = 333 + a; i < 625; i = i + a) {
            g.drawLine(i, 365, i, 361);
        }

        g.drawString(player.getHp() + "/" + player.getMaxHp(), 27, 363);
        g.drawString(player.getAp() + "/" + player.getMaxAp(), 335, 363);
        g.drawString(enemy.getHp() + "/" + enemy.getMaxHp(), 27, 398);

        g.setColor(Color.black);
        g.drawRoundRect(25, 350, 292, 15, 7, 7);
        g.drawRoundRect(333, 350, 292, 15, 7, 7);
        g.drawRoundRect(25, 385, 292, 15, 7, 7);
        //paintComponents(g);
    }

    /**
     * main method for educational purpose only
     * @param args commandline args
     */
    public static void main(String[] args) {
        System.out.println("before");

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        Player player = new Player(150, 80, 8, .6);
        Monster enemy = new Monster(100, 10);
        FightClub f = new FightClub(player, enemy);

        System.out.println("after");
    }
}


