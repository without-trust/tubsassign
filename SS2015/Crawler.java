import java.io.IOException;
import java.util.Scanner;
import java.util.InputMismatchException;
import javax.swing.*;
/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * In diesem Programm bewegt sich der Spieler auch einer zufaelligen Karte von einem Startpunkt zu einem Ziel
 * und muss auf dem Weg gegen zufaellige Monster kaempfen.
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Crawler {

    /** savefile to store the games progress */
    public static final String SAVEFILE = "player.save";

    /**
     * main-Methode
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Player spieler = null;
        Scanner s = new Scanner(System.in);
        // here we decide to load an old player or to create a new one
        System.out.println("Do you want to start a new Game [y/n]");

        switch (s.next().charAt(0)) {
            case 'y':
            case 'Y':
            case 'j':
            case 'J':
                System.out.println("Please wait for a new Human to be created...");
                spieler = new Player(150, 80, 8, 0.6);
                System.out.println("Player created...");
                break;
            default:
                System.out.println("loading player from '" + SAVEFILE + "'");
                try {
                    spieler = SaveGameManager.loadObject(SAVEFILE);
                } catch (IOException e) {
                    System.out.println("Crawler: No such file or directory");
                    System.exit(1);
                }
                System.out.println("player loaded...");
        }

        System.out.println("SPIELER: " + spieler);
        System.out.println("Generating map...");

        RecursiveBacktracker rb = new RecursiveBacktracker(21, 21);
        char [][] mapData = rb.getMap();
        Level m = new Level(mapData);
        System.out.println("Map generating completed...");

        while (!spieler.isDefeated()) {  // !!Achtung, Endlosschleife!!
            round(spieler, m);
        }
        System.out.println("Plötzlich spuerst du einen scharfen Schmerz in der Brust.\n"
            + "Du kannst nicht mehr atmen und stuerzt zu Boden. \n"
            + "Bevor dir schwarz vor Augen wird, siehst du den Pfeil, der zwischen deinen Rippen steckt.\n");
    }

    /**
     * questbuddy is your trader on the map
     * @param player the player
     */
    public static void questBuddy(Player player) {
        //System.out.println("This is a robbery, give me all your questitems!");
        System.out.println("Möchtest du neue Quests abholen (1) oder Quests abgeben (2)?");
        Scanner sc = new Scanner(System.in);
        String in = sc.nextLine();
        GenericLinkedList<Quest> q;
        int i;
        Quest qu;
        switch (in) {
            case "1":
                //display available Quests
                q = player.getNewQuests();
                System.out.println(q.toString()
                    + "\nWelche?");
                //choose Quest
                try {
                    i = sc.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Versuch es wenigstens eine Zahl einzugeben..!");
                    break;
                }

                try {
                    qu = q.getItem(i);
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("So wird das nichts!");
                    break;
                }
                //activate Quest
                qu.activate();
                questBuddy(player);
                break;

            case "2":
                //display completable Quests
                q = player.getItemCompletedQuests();
                System.out.println(q.toString()
                    + "\nWelche?");
                //choose Quest
                try {
                    i = sc.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Versuch es wenigstens eine Zahl einzugeben..!");
                    break;
                }

                try {
                    qu = q.getItem(i);
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("So wird das nichts!");
                    break;
                }
                //complete Quest
                String qitem = qu.getItem();
                int j = 0;
                // while (j < qu.getQuantity()) {
                for (int k = 0; (k < player.getInventory().length() && j < qu.getQuantity()); k++) {
                    Item item = player.getInventory().getItem(k);
                    if (item.getName().equals(qitem)) {
                        player.getInventory().delete(item);
                        j++;
                        k--;
                    }
                }
                //}
                qu.complete();
                questBuddy(player);
                break;

            default:
                System.out.println("Bis bald!");
                break;
        }
    }


    /**
     * Spielrunde
     *
     * @param spieler Spieler
     * @param m karte
     */
    public static void round(Player spieler, Level m) {
        
        Scanner sc = new Scanner(System.in);
        m.output();
        char f; //Art des neuen Feldes
        char[] action = {0};
        do {
            String in = sc.nextLine();
            if (in.length() == 1) {
                action = in.toCharArray();
            }
            if (action[0] > 90) {
                action[0] = (char) ((int) (action[0]) - 32);
            }
            f = m.move(action[0]);
            if (f == 'I') {
                System.out.println(spieler.getInventory().toShortString() 
                        + "Geld: " + spieler.getGold() + "EUR");
            }
            if (action[0] == 'L') {
                System.out.println(spieler.questlog());
            }
            if (action[0] == 'T') {
                System.out.println("See you soon...");
                try {
                    SaveGameManager.saveObject(spieler, SAVEFILE);
                } catch (IOException e) {
                    System.out.println("Fehler beim Schreiben der Datei:" + e);
                }
                System.exit(0);
            }
        } while (f == 'N');

        fieldReaction(f, spieler, m);
    }

    /**
     * Reaktion auf Feldtyp
     *
     * @param f Feldtyp
     * @param spieler Spieler
     * @param m Karte
     */
    public static void fieldReaction(char f, Player spieler, Level m) {
        Scanner sc = new Scanner(System.in);
        switch (f) {
            case 'O':  //Brunnen
                System.out.println(
                    "Du kommst an einen Brunnen mit Heilwasser." + "\n"
                    + "Lebenspunkte: " + spieler.getHp() + ", Traenke: " + spieler.getRemainingItemUses() + "\n"
                    + "Möchtest du dich vollständig heilen (1) oder deine Tränke auffuellen (2)?");
                String in = sc.nextLine();

                if (in.equals("1")) {
                    System.out.println("Deine Lebens- und Aktionspunkte wurden aufgefuellt.");
                    spieler.fountainHeal();
                } else if (in.equals("2")) {
                    if (spieler.getRemainingItemUses() < spieler.getMaxItems()) {
                        spieler.restoreItem();
                    }
                    System.out.println("Deine Tränke  wurden aufgefuellt. Du traegt jetzt "
                        + spieler.getRemainingItemUses() 
                        + (spieler.getRemainingItemUses() == 1 ? " Item." : " Items."));
            //    } else {
            //        m.skipFountain();
                } // end of if-else
                break;
                
            case 'o':  //verbrauchter Brunnen
                System.out.println("Du kommst an einen ausgetrockneten Brunnen.");
                break;
                    
            case 'T':  //Schmiede
                System.out.println(""
                    + "Du stehst vor einer Schmiede. Aus dem Innern ertönen Schreie.\n"
                    + "Du gehst hinein und siehst, dass der Schmied von einem Raeuber bedroht wird.\n"
                    + "Als er dich sieht, ergreift der Raeuber die Flucht.\n"
                    + "Zum Dank schenkt dir der Schmied ein neues Schwert, mit dem du noch staerker"
                    + " angreifen kannst.");
                spieler.smith();
                break;
                    
            case 't': //verbrauchte Schmiede
                System.out.println("Du sehtst vor einer Schmiede. Aus dem Innern sind Hammerschlaege zu hoeren.");
                break;
            case 'Q':
                questBuddy(spieler);
                break;
            case 'Z': //Ziel

                // XXX TODO check wether or not all quests are completed
                if (spieler.getCompletedQuests().length() < spieler.getQuests().length()) {
                    System.out.println("LALALALA, mach die Quests fertig!");
                    break;
                }

                System.out.println(
                    "_______________________________________________\n"
                    + "_______________________________________________\n"
                    + "                              .\'\'.          \n"
                    + "    .\'\'.             *\'\'*    :_\\/_:     .   \n"
                    + "   :_\\/_:   .    .:.*_\\/_*   : /\\ :  .\'.:.\'.\n"
                    + "  .\'\'.: /\\ : _\\(/_   \':\'* /\\ *  : \'..\'.  -=:o:=-\n"
                    + " :_\\/_:\'.:::. /)\\*\'\'*      * \'.\\\'/.\'_\\(/_\'.\':\'.\'\n"
                    + " : /\\ : :::::   \'*_\\/_*      -= o =- /)\\    \'  *\n"
                    + "  \'..\'  \':::\'    * /\\ *  *   .\'/.\\\'.  \'         \n"
                    + "   *          *..*          :               \n"
                    + "Du konntest aus der Stadt der Verdammten entkommen!\n");
                System.exit(0);
                break;
                
            case 'B': //Kampf
                System.out.println("Du wirst angegriffen!");
        
                Monster gegner;
                int r = (int) (Math.random() * 3);
                int mmaxHp = (int) (Math.random() * 50 + 100);
                double mhit = 0.5 + (150.0 - mmaxHp) / 100.0;

                if (r == 0) {
                    gegner = new Monster1(mmaxHp, mhit);
                } else if (r == 1) {
                    gegner = new Monster2(mmaxHp, mhit);
                } else {
                    gegner = new Monster(mmaxHp, mhit);
                }

                spieler.setHitChance(0.7 - (150 - gegner.getStrength()) / 200);
                FightClub ui = new FightClub(spieler, gegner);
                Sync.waitForBattleEnd();
                break;
                
            case 'b': //ehemaliger Kampfplatz
                System.out.println("Das Gras ist zertreten, so als haette es einen Kampf gegeben.");
                break;

            case 'H': //Haendler
                m.useShop(spieler);
                break;
            default: // default required by checkstyle ruleset
            

        } // end of switch
    }
}
