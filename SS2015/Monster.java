/**
 * Dies ist die aktuelle Pflichtaufgabe.
 * Diese Datei beinhaltet die Monster-Methode.
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 2a
 * @author Steffen Richter 4388227 Gruppe 2a
 * @author Philipp 4569839 Gruppe 5b
 */
class Monster extends Entity {    
    
    /**
     * Konstruktor
     * @param maxHp Die maximale Anzahl an Lebenspunkten
     * @param hitChance Die Trefferwahrscheinlichkeit
     */
    public Monster(int maxHp, double hitChance) {
        setMaxHp(maxHp);
        setHp(maxHp);
        setHitChance(hitChance);
        setName("Kultist");
        setAtk(getRate());
        setInventory(new GenericLinkedList<Item>());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            getInventory().insert(new Item());
        }
        setGold((int) (Math.random() * 133.7));
    }
    
    /**
     * Konstruktor (Standardwerte fuer das Monster) 
     */
    public Monster() {
        setMaxHp(125);
        setHp(getMaxHp());
        setHitChance(0.5);
        setName("Kultist");
        setAtk(getRate());
        setInventory(new GenericLinkedList<Item>());
        int r = (int) (Math.random() * 3);
        for (int i = 0; i < r; i++) {
            getInventory().insert(new Item());
        }
        setGold((int) (Math.random() * 133.7));
    }  
    
    /**
     * Angriff des Monsters auf den Spieler
     * @param player Angegriffener Spieler
     * @return Integer d, der bei Teffer den Schaden angibt, sonst -1    
     */
    public int attack(Player player) {      
        double r = Math.random(); 
        int d;        
        System.out.println("Monster greift an!");         
        if (r < this.getHitChance()) {              
            d = (int) ((0.0 + this.getAtk()) * (Math.random() + 1));
            player.takeDamage(d);            
            System.out.println("Du wurdest getroffen!");                     
        } else {  
            d = -1;                        
            System.out.println("Monster hat dich verfehlt!");                     
        }         
        return d;
    }
    
    /**
     * gibt Lebenspunkte zuruek
     * @return Lebenspunkte
     */
    public int getStrength() {
        return getHp();
    }
    
}
