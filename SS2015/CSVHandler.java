import java.io.*;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 * class for parsing csv files to structures that are compatible with the game
 *
 * @author Eva Vanessa Bolle 4528650 Gruppe 5b
 * @author Philipp 4569839 Gruppe 5b
 */
class CSVHandler {
    /** delimiter is defined here */
    private String delimiter = ",";

    /** define number of commentline at the beginning that should not be parsed */
    private int commentlines = 1;

    /** buffered reader for reading file */
    private BufferedReader br;

    /**
     *  Constructor
     *  @param path path to the file that is to be parsed
     *  @throws FileNotFoundException Exception
     */
    public CSVHandler(String path) throws FileNotFoundException {
        this.br = new BufferedReader(new FileReader(path));
    }

    /**
     * parses quests from the given file
     * @param questLst List of quests the new ones are supposed to be added
     * @throws IOException Exception
     */
    public void getQuests(GenericLinkedList<Quest> questLst) throws IOException {
        String name;
        String prequest;
        String item;
        int quantity;

        String line = null;

        for (int i = 0; i < this.commentlines; i++) {
            String comment = this.br.readLine();
        //    System.out.println("comment: " + comment);
        }

        while ((line = this.br.readLine()) != null) {
            String[] arr = line.split(delimiter);
            
            name = arr[0].trim();
            prequest = arr[1].trim();
            item = arr[2].trim();
            quantity = (int) Double.parseDouble(arr[3]);
            
            questLst.insert(new Quest(name, prequest, item, quantity));
        }
    }

    /**
     * gets items from file and insertes them in the itmLst list
     * @param itmLst List of items the new ones are supposed to be added
     * @throws IOException Exception
     */
    public void getItems(GenericLinkedList<Item> itmLst) throws IOException {

        // needed for item creation
        String name;
        int value;
        int weight;

        String tmpline = null;

        for (int i = 0; i < this.commentlines; i++) {
            String comment = this.br.readLine();
        //    System.out.println("comment: " + comment);
        }

        while ((tmpline = this.br.readLine()) != null) {
            //System.out.println("raw" + tmpline);
            String[] tmparr = tmpline.split(this.delimiter);

            // for (String s : tmparr) {
            //    System.out.println("split" + s);
            // }
            
            // here we just assume that all input is sane and there are no bad
            // people in this world that put unexpected stuff into applications
            name = tmparr[0].trim();
            value = (int) Double.parseDouble(tmparr[1]);
            weight = (int) Double.parseDouble(tmparr[2]);

            itmLst.insert(new Item(name, value, weight));
        }
    }

    /**
     * main method for testing this class
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        System.out.println("CSVHandler TEST");

        // items

        GenericLinkedList<Item> items = new GenericLinkedList<Item>();

        try {
            CSVHandler h = new CSVHandler("item.csv");
            h.getItems(items);
        } catch (FileNotFoundException e) {
            System.out.println("This is not the file you were looking for: " + e);
        } catch (IOException e) {
            System.out.println("Is it a plane, is it a bird no it is an IOException: " 
                    + e);
        }
        
        System.out.println(items);
    
        // quests
        
        GenericLinkedList<Quest> quests = new GenericLinkedList<Quest>();

        try {
            CSVHandler h = new CSVHandler("quest.csv");
            h.getQuests(quests);
        } catch (FileNotFoundException e) {
            System.out.println("This is not the file you were looking for: " + e);
        } catch (IOException e) {
            System.out.println("Is it a plane, is it a bird no it is an IOException: " 
                    + e);
        }
        
        System.out.println(quests);
    }
}
