help:
	@echo -e "You may want to read the README file... ;)\nTo simply install run: '\u001B[32mmake setup\u001B[0m'"

setup:	checkstyle

checkstyle:
	wget --progress=bar -O ./misc/checkstyle-5.7-all.jar http://netcologne.dl.sourceforge.net/project/checkstyle/checkstyle/5.7/checkstyle-5.7-all.jar

clean:
	rm -rf ./misc/checkstyle-5.7-all.jar
