/**
 * This class implements the quicksort algorithm in a very specific way that is
 * taught in the legendary lectures of Fekete
 *
 * Runtime:
 *	Average:	O(n log n)
 *	Worst case:	O(n^2)
 *
 *	
 * Found a bug/improved something?
 * Nice peole report it back to me, or even send in a patch.
 *
 * Author: Philipp B 'philipp.b.610@googlemail.com'
 *
 * */

class QuicksortExample {

	/** print array to string */
	public static String pa2s(int a[]) {
		String out = "";

		for (int i=0; i<a.length; i++) {
			out += (a[i] + " ");
		}
		return out;
	}

	/**
	 * partition function that does the main work
	 * */
	public static int partition(int a[], int p, int r) {
	
		System.out.println("parition(" + (p+1) + ", " + (r+1) + ")");

		String out = "";
		
		// initialize temp for later swapping
		int temp;

		// set pivotelement
		int x = a[r];
		// set pointer i (j is defined in the loop)
		int i = p-1;
		int j;

		for (j=p; j < r; j++) {
			
			System.out.println("Compare: " + a[j] + " <= " + x);
			
			if (a[j] <= x) {
				// in case there are two numbers that need to
				// swapped, i needs to be increased
				i++;
				
				// swapping variables
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;

				out = pa2s(a) + " | " 
					+ (i+1) + " " 
					+ (j+1) + " " 
					+ x;
				out += " - swap a[" + (i+1) + "]=" + a[i]
					+ " and a[" + (j+1) + "]=" + a[j];
			} else {
				out = pa2s(a) + " | " 
					+ (i+1) + " " 
					+ (j+1) + " " 
					+ x;
				out += " - do nothing";

				// clear output comment out to enable/disable
				//out = "";
			}
			System.out.println("\t" + out);
		}


		System.out.println("final swap:");

		// swap after the parting, so that at least the element at p+1
		// is at the right position
		temp = a[i+1];
		a[i+1] = a[r];
		a[r] = temp;

		// output what is going on
		out = pa2s(a) + " | " + (i+1) + " " + (j+1) + " " + x;
		out += " - swap a[" + (i+2) + "]=" + a[i+1]
			+ " and a[" + (r+1) + "]=" + a[r];
		System.out.println("\t" + out);

		System.out.print("\n");

		// finally return position of currect element
		return i+1;
	}

	/** 
	 * quicksort function itself 
	 * */
	public static void quicksort(int a[], int p, int r) {
		//System.out.println("call quicksort(p="+p+", r="+r+")");
		if (p < r) {
			int q = partition(a, p, r);
			
			quicksort(a, p, q-1);
			quicksort(a, q+1, r);
		}
	}

	public static void main(String args[]) {
		// array to be sorted...
		//int a[] = {2,8,7,1,3,5,6,4};
		//int a[] = {4,2,8,1,7,6,5,3}; // blatt5.pdf
		//int a[] = {1,2,3,4,5,6,7,8};
		//int a[] = {4,17,8,3,12};
		int a[] = {14,3,7,1,2};

		System.out.println("INPUT:  " + pa2s(a));

		System.out.println("====\tSTART");
		quicksort(a, 0, a.length-1);
		System.out.println("====\tEND");

		System.out.println("OUTPUT: " + pa2s(a));

	}
}
