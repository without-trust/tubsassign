import java.util.Scanner;

public class Uebung2_aufgabe_9{
    public static void main(String[] args) {
        
        int a;
        double x = 1.00;
        double n;
        double erg;
        Scanner in = new Scanner(System.in);

        System.out.println("Wie lautet Ihre Zahl, von der die Kubikwurzel errechnet werden soll?");
        a = in.nextInt();
        System.out.println("Wie viele Durchläufe willst du probieren?");
        n = in.nextInt(); 
        
        int i = 1;
        while(i <= n){
            x = (((a / (x * x)) + 2 * x) * (1.00 / 3.00));
            System.out.println("Ihr Ergebnis ist: " + x);
            i++;
        }

    }
}
