public class klausurws1314_aufg3f {
    public static void main(String[] args) {
        int[] a = new int[6];

        System.out.println(a.length);
        for(int i = 0; i < a.length; i++) {
            a[i] = (11 - 2 * i) % 7;
            System.out.println(i + " = " + a[i]);
        }
        System.out.println("");
        for(int i = a.length - 1; i >= 0; i--) {
            a[i] = a[5 - a[i]];
            System.out.println(i + " = " + a[i]);
        }
    }
}
