class Faculty {
    public static void main(String[] args) {
        int n = 5;
        System.out.println("Result: f(" + n + ") == " + peter(n));
    }

    public static int peter(int n) {
        System.out.println("call f(" + n + ")");                        
        int fac = 1;

        if ( !(n == 1 || n == 0) ) {
            fac = (n * peter(n-1));
        }

        System.out.println("ret " + fac + " from f(" + n + ")");
        return fac;
    }
}
