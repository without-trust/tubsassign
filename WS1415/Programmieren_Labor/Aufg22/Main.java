class Main {
	public static void main(String[] args) {
		System.out.println(recurDigitSum(123456789));
	}

	public static int recurDigitSum(int x) {
		int ret = digitSum(x);

		System.out.println(x);
		
		while ( ret >= 10 ) {
			ret = recurDigitSum(digitSum(x));
		}

		return ret;
	}

	public static int digitSum(int x) {
		char[] ca = Integer.toString(x).toCharArray();

		int sum = 0;

		for (char c : ca) {
			sum += Character.getNumericValue(c);
		}

		return sum;
	}
}
