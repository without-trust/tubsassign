public class Naturals extends Sequence {
	private long counter;
	//private long limit_min;
	private long limit_max;

	public Naturals() {
		this.counter = 0;
		//this.limit_min = Long.MIN_VALUE;
		this.limit_max = Long.MAX_VALUE;
	}

	public Naturals(long counter) {
		this.counter = counter;
		//this.limit_min = Long.MIN_VALUE;
		this.limit_max = Long.MAX_VALUE;
	}
	
	public Naturals(long counter, /*long limit_min,*/ long limit_max) {
		this.counter = counter;
		//this.limit_min = limit_min;
		this.limit_max = limit_max;
	}


	/** returns wether or not there are more elements in that sequence */
	boolean moreElements() {
		try{
			long val = f(this.counter + 1);

			if( val > this.limit_max + 1
				/* || val < this.limit_min*/ ){
				return false;
			}

		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	/** retuns next element in sequence */
	long nextElement() {
		long ret = 0;

		try {
			ret = f(this.counter);
		} catch (Exception e){}
		
		if ( moreElements() ) {
			counter++;
		}
		
		return ret;
	}

	/** f just adds one to the parameter and returns safely */
	public static long f(long x) throws Exception {
		return longAdd(1, x);
	}

	public static long longAdd(long a, long b) throws Exception {
		// make sure a is smaller than b
		if ( a > b ) {
			return longAdd(b, a);
		}

		// comments are beautiful
		if (a < 0) {
			// check for MIN_VALUE
			if (b < 0) {
				if (Long.MIN_VALUE - b <= a) {
					return a + b;
				} else {
					throw new ArithmeticException();
				}	
			} else {
				// opposite signs are save to add
				return a + b;
			}
		} else {
			// check for MAX_VALUE
			if (b > 0) {
				if (a <= Long.MAX_VALUE - b) {
					return a + b;
				} else {
					throw new ArithmeticException();
				}
			} else {
				// opposite signs are save to add
				return a + b;
			}
		}
	}
	
	public String toString() {
		String out = "[";
		int count = 10;

		while(this.moreElements()) {
			out += this.nextElement();
			if(this.moreElements())
				out += ",";
			count--;
			if (count <= 0){
				out += "...";
				break;
			}
		}

		return out + "]";
	}
}
