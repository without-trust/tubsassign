abstract class Sequence {
	
	boolean moreElements() {
		return false;
	}
	
	long nextElement() {
		return 0;
	}

	public String toString() {
		return "";
	}
}
