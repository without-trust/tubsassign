class Main {
	public static void main(String[] args) {
		Sequence seq = new Naturals(0, 30);

		while( seq.moreElements() ) {
			System.out.print(seq.nextElement() + " ");
		}
		System.out.println(" ");

		Sequence p = new Naturals(0, 30);
		System.out.println(p);
	}
}
