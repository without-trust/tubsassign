class ShiftingBits {
	public static void main(String[] args) {
		int defaultValue = -(int) (Math.random()*Integer.MAX_VALUE);
		int revs = 32;
		int bits = 1;
		int x;
		
		System.out.print("\n\n<<\n");
		
		x = defaultValue;
		for (int i=0; i<revs; i++) {
			out(x);
			x = x << bits;
		}
		
		System.out.print("\n\n>>\n");

		x = defaultValue;
		for (int i=0; i<revs; i++) {
			out(x);
			x = x >> bits;
		}

		System.out.print("\n\n>>>\n");

		x = defaultValue;
		for (int i=0; i<revs; i++) {
			out(x);
			x = x >>> bits;
		}


	}

	public static void out(int x) {
		System.out.printf("%16d - %32s\n", x, Integer.toBinaryString(x));
	}
}
