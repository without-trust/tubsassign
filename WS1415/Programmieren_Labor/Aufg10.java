import java.util.Scanner;

class Aufg10 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		int[][] matrix = new int[2][3];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) {
				matrix[i][j] = s.nextInt();
				System.out.println(matrix[i][j]);
			}
		}

		// was ist im array, wird durch forschleife ausgegeben
		for (int i=0; i<2; i++) {
			for (int j=0; j<3; j++) {
				System.out.println("matrix["+i+"]["+j+"]"+matrix[i][j]);
			}
			System.out.println("");
		}

	}
}
