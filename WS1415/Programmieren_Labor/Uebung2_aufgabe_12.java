import java.util.Scanner;

public class Uebung2_aufgabe_12 {
    public static void main (String[] args) {
        Scanner s = new Scanner(System.in);
        int P;
        int p;
        int x = 0;

        System.out.println("Welche Zahl soll gegen geprüft werden?");
        P = s.nextInt();
        System.out.println("\n");
       
        //Primzahl überprüfung:
        boolean primtest = true;
        for(int t = 2; t < P; t++) {
            if ((P % t) == 0) {
                primtest = false;
            }
        }
        if (primtest) {
            for(p = 2; p <= P; p++) {
                boolean prim = true;
                for(int j = 2; j < p && prim; j++) {
                    if ((p % j) == 0) {
                        prim = false;
                    }
                }
                if (prim) {
                    while(P >= p + 2 * Math.pow(x,2)) {
                        if(P == p + 2 * Math.pow(x,2)) {
                            System.out.println("Dein Zahl wurde erfolgreich gegen getestet und lautet: \n"
                                + "" + P + " = " + p + " + 2 " + x + "²");
                            break;
                        } else {
                            x++;
                        }
                    }
                }
            }


        } else {
            System.out.println(P + " ist keine Primzahl");
        }
        
    }
}

