import java.util.Scanner;

public class Uebung1_aufgabe_7{
    public static void main(String[] args) {
		
        Scanner hoe = new Scanner(System.in);
        Scanner um = new Scanner(System.in);
        double h;
        double u;
        
        System.out.println("Gib deine Daten des Zylinders zum berechnen ein: \n"
                + "Höhe:");
        h = hoe.nextDouble();
        System.out.println("Umfang: ");
        u = um.nextDouble();
        //Berechnungen:
        //Durchmesser
        double d;
	d = u / Math.PI;
        //Fläche des Bodens
        double F;
        F = Math.PI * ((d / 2) * (d / 2));
        //Mantelfläche des Zylinders
        double M;
        M = u * h;
        //Gesamtfläche des Zylinders
        double G;
        G = 2 * F + M;
        //Volumen des Zylinders
        double V;
        V = F * h;
        	
        System.out.println("Ihr Zylinder hat folgender Maße: \n"
                + "Durchmesser des Bodens: " + d + "\n"
                + "Fläche des Bodens: " + F + "\n"
                + "Mantelfläche des Zylinders: " + M + "\n"
                + "Gesamtfläche des Zylinders: " + G + "\n"
                + "Volumen des Zylinders: " + V);

    }
}
