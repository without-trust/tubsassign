import java.util.Scanner;

public class Uebung5_aufgabe_22 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a;
        System.out.println("Welche Quersumme soll berechnet werden?");
        a = s.nextInt();
        System.out.println(quersumme_rek(a));
    }

    static int quersumme_iter(int a) {
        char[] b = Integer.toString(a).toCharArray();
        int quer = 0;
        for(int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
            int c = Character.getNumericValue(b[i]);

            quer += c;
        }
        System.out.println("");
        return quer;
    }
    static int quersumme_rek(int a) {
        int x = quersumme_iter(a);
        
        while (x >= 10) {
           x =  quersumme_rek(quersumme_iter(x));
        }
        
        return x;
    }
}
