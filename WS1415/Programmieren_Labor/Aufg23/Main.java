class Main {
	public static long newOnes(long t) {
		t -= 2;
		
		if ( t > 0 && t % 3 == 0 ) {
			return 3;
		}

		return 0;
	}

	public static long f(long t) {
		long no;
		long count = 0;
		for ( long i = 0; i < t; i++ ) {
			no = newOnes(i);
			for (int j = 0; j < no; j++) {
				count += f(t-i);
			}
		}

		return count + 1;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 35; i++) {
			System.out.printf("%50d - %10d\n", i, f((long)i));
		}
	}
}
