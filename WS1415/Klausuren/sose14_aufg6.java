import java.util.Scanner;

public class sose14_aufg6 {
    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.println("Geben Sie ihre Sphenische Zahl ein");
        n = s.nextInt();
        try {
	    test(n);
        } catch (IllegalArgumentException e) {
            System.out.println("Deine Sphenische Zahl kann nicht kleiner als 2 sein");
        }
    }

    public static int test(int n) {
        if (n < 2) {
            throw new IllegalArgumentException("kleiner als 2");
        } else {
            sphenisch(n);
        }
	return n;
    }

    public static boolean sphenisch(int n){
        int[] prim = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;
        boolean a = false;
        
        links: for(int i = 0; i < prim.length; i++){
            s1 = prim[i];
            mitte: for(int j = 1; j < prim.length; j++) {
                s2 = prim[j];
                rechts: for(int k = 2; k < prim.length; k++) {
                    s3 = prim[k];
                    if(s2 <= s3) {
                        continue;
                    } else if(n == (s1 * s2 * s3)) {
                        a = true;
                        return a;
                    } else if(n < (s1 * s2 * s3)) {
                        continue mitte;
                    } else if(k == prim.length - 1){
                        if(n > (s1 * s2 * s3)) {
                            continue mitte;
                        }
                    }
                }

                if(s1 <= s2) {
                    continue;
                } else if(n == (s1 * s2 * s3)) {
                    a = true;
                    return a;
                } else if(n < (s1 * s2 * s3)) {
                    continue links;
                } else if(j == prim.length - 1) {
                    if(n > (s1 * s2 * s3)) {
                        continue links;
                    }
                }

            }
            if(n == (s1 * s2 * s3)) {
                a = true;
                System.out.println(n + "ist eine Sphenische Zahl und wird wie folgt berechnet: " + s1 + "*" + s2 + "*" + s3);
                return a;
            } else if(n < (s1 * s2 * s3)) {
                System.out.println(n + " ist keine Sphenische Zahl");
                a = false;
                break;
            } else if(i == prim.length - 1) {
                if(n > (s1 * s2 * s3)) {
                    System.out.println(n + " ist keine Sphenische Zahl");
                    a = false;
                }
            }
        }
        return a;
    }

}
