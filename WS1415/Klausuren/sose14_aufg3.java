import java.util.Scanner;

public class sose14_aufg3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String auswahl;
        System.out.println("Welche Aufgabe soll gelöst werden? \n"
                + "(1) Aufgabe 3a \n"
                + "(2) Aufgabe 3b \n"
                + "(3) Aufgabe 3c \n"
                + "(4) Aufgabe 3d \n"
                + "(5) Aufgabe 3e \n"
                + "(6) Aufgabe 3f");
        auswahl = s.nextLine();

        switch (auswahl) {
            case "1": a();
                    break;
            case "2": b();
                    break;
            case "3": c();
                    break;
            case "4": d();
                    break;
            case "5": e();
                    break;
            case "6": f();
                    break;
            default: System.out.println("Gebe eine richtige Eingabe ein... Idiot");
                     break;
        }
    }

    public static void b(){
        int x = 2;
        int y = 2;
        for(x = 4; x > 0; x = x + y - 2) {
            y = y + 1;
            if (y <= 5) {
                System.out.println("If = t, x= " + x + " y= " + y);
                continue;
            }
            if (x >= 5) {
                System.out.println("if 2 = t");
                break;
            }
            y = x * y;
            System.out.println("X = " + x + " Y = " + y);
        }
        System.out.println("b terminiert mit x = " + x + " und y = " + y);
    
    }

    public static void a() {
        int x = 6;
        int y = 1;
        do {
            x = x + y;
            switch (x % 4) {
                case 0: y = +4;
                        break;
                case 1: y = +5;
                        break;
                case 2: y = +1;
                        break;
                case 3: y = -1;
                        break;
                default: y = +2;
                         break;
            }
            System.out.println("X= " + x + " Y= " + y);
        } while (y < 2);
        System.out.println("a terminiert mmit x = " + x + " und y = " + y);
    }
    public static void c() {
        int x = 11;
        boolean b = false;
    
        if(2 == (x >> 2)) {
            b = true;
            System.out.println("3c ist Boolean mit dem wert: " + b);
        } else {
            System.out.println("3c ist Boolean mit dem Wert: " + b);
        }
        
    }

    public static void d() {
        int x = 11;
        x = x % 3 + x / 3 + x - 3;
        System.out.println("Das Ergbnis ist: " + x);
    }

    public static void e() {
        int x = 11;
        x = x^(x-1);
        System.out.println("Das Ergebnis ist: " + x);
    }

    public static void f() {
        int[] a = new int[6];

        for (int i = 0; i < a.length; i++) {
            a[i] = (2 * i + 5) % 6 - 1;
            System.out.println(a[i]);
        }
        System.out.println("");
        for (int i = a.length - 1; i >= 0; i--) {
            a[i] = 1 + a[1 + a[i]];
            System.out.println(a[i]);
        }
    }
} 
