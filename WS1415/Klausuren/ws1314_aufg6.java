public class ws1314_aufg6 {
    public static void main(String[] args) {
        int[] a = {5, 2, -5, 1, 3, -5, -2, 4, 0};     
	
	// try catch, um eine mögliche exception zu fangen
	try{
	    specialSort(a);
        } catch (IllegalArgumentException e) {
            System.out.println("Dein Array sollte schon zahlen haben...");
        }

	
	// array ausgeben
	for (int i : a) {
		System.out.print(i + " ");
	}
	System.out.println();
        
    }

    // a wird als referenzobjekt übergeben
    public static void specialSort(int[] a) throws IllegalArgumentException {
       	// wenn a null ist wird eine exception geschmissen
	if (a == null) {
            throw new IllegalArgumentException();
        }

	// XXX Manipulationen am array hier
    }
}
