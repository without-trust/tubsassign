/**
 * This class is supposed to calculate and verify an ISBN13's checksum.
 * @author Daniel Thümen    4523176 Gruppe 7a
 * @author Philipp Bartsch  4569839 Gruppe 7a
 * */

public class Isbn {
    /**
     *  Main method, present in most java programs.
     *  @param args startingparameters passed by commandline
     * */
    public static void main(String[] args) {
        
        // input validation
        if ( args.length != 1 ) {
            System.out.println("Illegal or missing parameter!\nusage: java isbn <ISBN>");
            System.exit(1);
        }

        // initializing variables
        String isbn = args[0];
        int checksum = 0, temp = 0, sum = 0, c = 0;

        // calculate checksum
        for ( int i = 0, comp = isbn.length() - 1; i < comp; i++ ) {
            temp = Character.getNumericValue(isbn.charAt(i));
            if ( temp >= 0 && temp <= 9  ) {
                sum += (c + 1) % 2 == 0 ? temp * 3 : temp;
                c++;
            } 
        }
        checksum = 10 - (sum % 10);
        
        if ( c != 12 ) {
            System.out.println("Eingegebene ISBN-13 sollte 13 stellig sein.");
            System.exit(1);
        }

        // output result
        System.out.println( checksum == Character.getNumericValue(isbn.charAt(isbn.length() - 1)) 
                ? isbn + "ist eine gültige ISBN." 
                : isbn + " ist eine fehlerhafte ISBN\nPrüfsumme sollte '" + checksum + "' sein.");
    }
}
