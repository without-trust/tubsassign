import java.util.Scanner;
import java.util.ArrayList;


/** This class is supposed to manage the objects and events in our small RPG.
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Main {
    /** Main method
     *  @param args commandline parameters
     */
    public static void main(String[] args) {
        int en = (int) (Math.random() * 4);
        Scanner s = new Scanner(System.in);
        Scanner enter = new Scanner(System.in);
        String welcome = 
              "    ___   _____ ____    ____    ____   ____\n"
            + "   / _ \\ |  ___|  _ \\  |  _ \\  |  _ \\ / ___|  \n"
            + "  | | | || |_  | |_) | | |_) | | |_) | |  _   \n"
            + "  | |_| ||  _|_|  _ < _|  _ < _|  __/| |_| |_ \n"
            + "   \\___(_)_| (_)_| \\_(_)_| \\_(_)_| (_)\\____(_)\n\n";
        
        String noob = "\u001B[31m\n"
                    + "  ▄████  ▄▄▄       ███▄ ▄███▓▓█████     ▒█████   ██▒   █▓▓█████  ██▀███       \n"
                    + " ██▒ ▀█▒▒████▄    ▓██▒▀█▀ ██▒▓█   ▀    ▒██▒  ██▒▓██░   █▒▓█   ▀ ▓██ ▒ ██▒     \n"
                    + "▒██░▄▄▄░▒██  ▀█▄  ▓██    ▓██░▒███      ▒██░  ██▒ ▓██  █▒░▒███   ▓██ ░▄█ ▒     \n"
                    + "░▓█  ██▓░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄    ▒██   ██░  ▒██ █░░▒▓█  ▄ ▒██▀▀█▄       \n"
                    + "░▒▓███▀▒ ▓█   ▓██▒▒██▒   ░██▒░▒████▒   ░ ████▓▒░   ▒▀█░  ░▒████▒░██▓ ▒██▒     \n"
                    + " ░▒   ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░   ░ ▒░▒░▒░    ░ ▐░  ░░ ▒░ ░░ ▒▓ ░▒▓░     \n"
                    + "  ░   ░   ▒   ▒▒ ░░  ░      ░ ░ ░  ░     ░ ▒ ▒░    ░ ░░   ░ ░  ░  ░▒ ░ ▒░     \n"
                    + "░ ░   ░   ░   ▒   ░      ░      ░      ░ ░ ░ ▒       ░░     ░     ░░   ░      \n"
                    + "      ░       ░  ░       ░      ░  ░       ░ ░        ░     ░  ░   ░          \n"
                    + "                                                     ░                        \n"
                    + "\u001B[0m\n";
        
        
        
        String goon = "============================PRESS ENTER TO CONTINUE============================ \n";
        
        /*
         * initialize environment
         */

        System.out.print(welcome);
        ArrayList<Entity> enemys = new ArrayList<Entity>();
        enemys.add(new Entity(50, 6, 0.5, "Hetler", 0));
        enemys.add(new Entity(60, 7, 0.5, "GLaDOS", 0));
        enemys.add(new Entity(70, 8, 0.6, "Bowser", 0));
        enemys.add(new Entity(1000, 9, 0.8, "God", 0));
        
        Entity enemy = enemys.get(en);
        System.out.println("God: \tGreetings Warrior! \n" 
                + "\tWould you like to tell me your name  my little friend?");
        System.out.print("You: \tMy friend call me: ");
        Entity player = new Entity((int) ((Math.random() * 50) + 51), 
                (int) ((Math.random() * 10) + 5), 0.65, s.next(), (int) (Math.random() * 21));
        player.setMaxAP(20);
        
        
        System.out.println("\t\t... only I ain't got no friends.");
        System.out.println("God: \tOkay " + player.getName() + "!");
        System.out.println("\n\tJust let me see what your stats are real quick: " 
                + "\n\t\tMaximum HP:" + player.getMaxHp()
                + "\n\t\tHitpoints:" + player.getHp()
                + "\n\t\tAttack damage:" + player.getAtk()
                + "\n\t\tMana:" + player.getAP());
        System.out.println("\nGod: \tAre you ready for your first quest?");
        s.next();
        System.out.println("Great! Here is your first Quest: Kill " + enemy.getName());
        System.out.println("");
        System.out.println("");
        System.out.print(goon);
    
        enter.nextLine();
        
        Effect heal = new Effect(8, "divine blessing", 0);
        Item pot = new Item("Healing Potion", heal);
        player.addItem(pot);
        player.addItem(pot);
        player.addItem(pot);
        player.addItem(pot);
        
        
        Mana ffart = new Mana("Fire-Fart", 5);
        Effect fart = new Effect(-4, "Fart", 0);
        ffart.addEffect(fart, true, true);
        player.addMana(ffart);
        
        Mana absorb = new Mana("Absorb Mana", 6, 0.8);
        Effect loss = new Effect(0, "loss of the blue gold", -5);
        Effect gain = new Effect(0, "gain of the blue gold", 5);
        absorb.addEffect(loss, true, true);     // success
        absorb.addEffect(gain, true, false);    // success
        absorb.addEffect(loss, false, false);   // failed
        player.addMana(absorb);
        
        Mana dinstant = new Mana("Instand Kill", 20, 0.5);  // create a spell with no effects 
                                                            //(you have to add them manually, see next lines)
        Effect instant = new Effect(-99999, "dead", 0);     // create Effect
        dinstant.addEffect(instant, false, false);          // defines the backfire
        dinstant.addEffect(instant, true, true);            // defines what happens when you hit correctly
        player.addMana(dinstant);                           // add completed spell to player    
        
        
        if (Fightclub.battle(player, enemy)) {
            System.out.println("");

            
        } else {
            System.out.println("");
        }
    }
}
