/** Sandbox is for educational purpose only!
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
class Sandbox {
    /** main method
     *  @param args commandline parameters
     */
    public static void main(String[] args) {
        Entity p1 = new Entity(1000, 10, 0.85, "player1", 12);
        Entity p2 = new Entity(100, 10, 0.85, "player2", 12);

        Effect blessing = new Effect(13, "big blessing");
        Item pot = new Item("healing potion", blessing);

        Effect lesserHealing = new Effect(3, "lesser blessing");
        Item smallpot = new Item("lesser healing potion", lesserHealing);

        p1.addItem(pot);
        p1.addItem(smallpot);
        p1.addItem(pot);
        p1.addItem(smallpot);
        p1.addItem(pot);

        Item test = new Item("testing");
        p1.addItem(test);
        if (p1.hasItem(test)) {
            System.out.println("has item");
        } else {
            System.out.println("nope");
        }


        p1.setHp(45);
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());
        p1.heal();
        System.out.println(p1.getHp());


        System.out.println(" == == == ");

        Effect specialheal = new Effect(30, "special");
        Item specialpot = new Item("special pot", specialheal);

        p1.addItem(specialpot);

        System.out.println(p1.getHp());
        p1.consumeItem(specialpot);
        System.out.println("still running");
        System.out.println(p1.getHp());
        p1.consumeItem(specialpot);
        System.out.println(p1.getHp());



        // add a frw items
        Item i1 = new Item("item 1");
        Item i2 = new Item("item 2");

        p1.addItem(i1);
        p1.addItem(i2);
        p1.addItem(i1);

        System.out.println(" -- Inventory -- ");
        for (Item item : p1.getInventory()) {
            System.out.println("itemname: " + item.getName() + " (" + p1.countItem(item) + ")");
        }
    }
}
