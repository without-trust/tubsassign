/** Effects are supposed to increase health, atk, etc.
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Effect {
    /** hp and atk modification */
    private int hp, ap;
    /** effect name */
    private String name;

    /** Constructor
     */
    Effect() {
        this.hp = 0;
        this.name = "";
        this.ap = 0;
    }

    /** Constructor
     * @param hp hitpoint value modificator
     * @param ap ability points
     */
    Effect(int hp, int ap) {
        this.hp = hp;
        this.name = "";
        this.ap = ap;
    }

    /** Constructor
     * @param hp hitpoint value modificator
     * @param name effect name
     */
    Effect(int hp, String name) {
        this.hp = hp;
        this.name = name;
        this.ap = 0;
    }


    /** Constructor
     * @param hp hitpoint value modificator
     * @param name effect name
     * @param ap ability points
     */
    Effect(int hp, String name, int ap) {
        this.hp = hp;
        this.name = name;
        this.ap = ap;
    }

    /** returns effect name
     *  @return name of effect
     */
    public String getName() {
        return this.name;
    }

    /** setter for effect name
     *  @param name effect name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** simply returns hitpoint modification value
     *  @return hp hitpoint modification value
     */
    public int getHp() {
        return this.hp;
    }

    /** set hp modification value
     *  @param hp hitpoint modification value
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /** set AP modification
     *  @param ap ability points modification value
     */
    public void setAp(int ap) {
        this.ap = ap;
    }


    /** really all it does is returning 'em AP
     *  @return ap ability...
     */
    public int getAp() {
        return this.ap;
    }
}
