import java.util.Scanner;

/** FIGHTCLUB RULES:
 *  1st RULE: You do not talk about FIGHT CLUB
 *  2nd RULE: You DO NOT talk about FIGHT CLUB
 *  3rd RULE: If someone says "stop" or goes limp, taps out the fight is over
 *  4th RULE: Only two guys to a fight
 *  5th RULE: One fight at a time
 *  6th RULE: No shirts, no shoes
 *  7th RULE: Fights will go on as long as they have to
 *  8th RULE: If this is your first night at FIGHT CLUB, you HAVE to fight
 *  
 *  Fightclub manages fights between players
 *  
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Fightclub {

    /** ascii art saved as attribute */
    private static String goon = "============================PRESS ENTER TO CONTINUE============================ \n";
    /** ascii art saved as attribute */
    private static String nerd =
                      "\u001B[31m \n"
                    + "▓█████▄  ▄▄▄       ███▄ ▄███▓ ███▄    █     ███▄    █ ▓█████  ██▀███  ▓█████▄ \n"
                    + "▒██▀ ██▌▒████▄    ▓██▒▀█▀ ██▒ ██ ▀█   █     ██ ▀█   █ ▓█   ▀ ▓██ ▒ ██▒▒██▀ ██▌\n"
                    + "░██   █▌▒██  ▀█▄  ▓██    ▓██░▓██  ▀█ ██▒   ▓██  ▀█ ██▒▒███   ▓██ ░▄█ ▒░██   █▌\n"
                    + "░▓█▄   ▌░██▄▄▄▄██ ▒██    ▒██ ▓██▒  ▐▌██▒   ▓██▒  ▐▌██▒▒▓█  ▄ ▒██▀▀█▄  ░▓█▄   ▌\n"
                    + "░▒████▓  ▓█   ▓██▒▒██▒   ░██▒▒██░   ▓██░   ▒██░   ▓██░░▒████▒░██▓ ▒██▒░▒████▓ \n"
                    + " ▒▒▓  ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░ ▒░   ▒ ▒    ░ ▒░   ▒ ▒ ░░ ▒░ ░░ ▒▓ ░▒▓░ ▒▒▓  ▒ \n"
                    + " ░ ▒  ▒   ▒   ▒▒ ░░  ░      ░░ ░░   ░ ▒░   ░ ░░   ░ ▒░ ░ ░  ░  ░▒ ░ ▒░ ░ ▒  ▒ \n"
                    + " ░ ░  ░   ░   ▒   ░      ░      ░   ░ ░       ░   ░ ░    ░     ░░   ░  ░ ░  ░ \n"
                    + "   ░          ░  ░       ░            ░             ░    ░  ░   ░        ░    \n"
                    + " ░                                                                     ░      \u001B[0m\n\n";

    /** In Battle wird der Fightclub ausgefhrt siehe Fightclub regeln
     * Battle is a fairfight between two players, and they don't talk about it!
     * @param player humancontrolled entity
     * @param enemy compuercontrolled enetity
     * @return wether or not the human conntolled entity has won
     */ 
    public static boolean battle(Entity player, Entity enemy) {
        int zahl1;
        String str = "26thoct2001";
        Scanner actionbutton = new Scanner(System.in);
        Scanner enter2 = new Scanner(System.in);
        
        /* This loop is the fight system, its an easy switch case system. */
        while (true) {
            System.out.println("\u001b[2J"); // just a little ANSI magic to reset the screen
            System.out.println("= = = = = = = = = = = = = = = = = = = = = = " 
                    + "= = = = = = = = = = = = = = = = = = = = = = = =");
            System.out.println("Your Stats: MaxHP: " + player.getMaxHp() + " | HP: " 
                + player.getHp() + " | Attack damage: " + player.getAtk() + " | Mana: " + player.getAP());
            System.out.println("Enemy Stats: HP: " + enemy.getHp() + " | Attack damage: " + enemy.getAtk());
            System.out.println("");
            System.out.println("Actionmenu!\nChoose 1 of the given options.\n" 
                    + "1: Attack!\n2: Item\n3: Run Away like a little chicken!\n4: Special Attacks");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            /*Hier werden unnötige eingaben abgefangen und zu -1 korrigiert siehe Ergebnis bei Case -1 */
            str = actionbutton.next();
            zahl1 = (str.matches("[0-9]*")) ? Integer.parseInt(str) : -1;
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\nYou've choose Number:" + zahl1 
                    + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            
            switch (zahl1) {
                case -1:
                    System.out.println("Damn noob choose the right buttons!");
                    break;
                case 1:
                    System.out.println(player.getName() + " tries to attack " + enemy.getName());
                    System.out.println(enemy.getName() + " has " + enemy.getHp() + " HP.");
                    int dmgdealt = player.attack(enemy);
                    if (dmgdealt == 0) {
                        System.out.println("Damn, why you don't hit him?!....");
                    } else {
                        if (enemy.getHp() >= 0) {
                            System.out.println(player.getName() + " attacks " + enemy.getName() 
                                + ", inflicted dmg: " + dmgdealt
                                + ", hitpoints left: " + enemy.getHp());
                        } else {
                            System.out.println(player.getName() + " attacks " + enemy.getName() 
                                + ", inflicted dmg: " + dmgdealt
                                + ", hitpoints left: 0");
                            
                        }
                        System.out.println("Wooohooo you hit " + enemy.getName() + " right in the face!");
                    }
                    break;
                case 2:
                    if (player.getInventory().size() == 0) {
                        System.out.println("--------------------------\nYou don't have any Items..\n" 
                                + "--------------------------\n");
                        break;
                    }
                    System.out.println("-----------------------------\nYou have the following Items:");
                    for (int i = 0; i < player.getInventory().size(); i++) {
                        System.out.println(i + ": " + player.getInventory().get(i).getName() 
                            + " (" + player.countItem(player.getInventory().get(i)) + ")");
                    }
                    System.out.println("-----------------------------");
                    str = actionbutton.next();
                    zahl1 = (str.matches("[0-9]*")) ? Integer.parseInt(str) : -1;
                    System.out.println("-----------------------------");
                    if (!(zahl1 >= 0 && zahl1 < player.getInventory().size())) {
                        System.out.println("-----------------------------\n" + player.getName() 
                                + " could not find the Item.\n-----------------------------\n");
                        break;
                    }
                    System.out.println("You've choose Item: " + player.getInventory().get(zahl1).getName());
                    System.out.println("-----------------------------");
                    player.consumeItem(player.getInventory().get(zahl1));
                    System.out.println("Your Hit Points grew up to: " + player.getHp() 
                        + "-----------------------------\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                    break;
                case 3:
                    System.out.println(player.getName() + " run away like a chicken! Bokk Bokkk Boooookkkk");
                    return false;
                case 4:
                    System.out.println("-----------------------------\nYou have the following Special Attacks:");
                    for (int i = 0; i < player.getMana().size(); i++) {
                        System.out.println(i + ": " + player.getMana().get(i).getName());
                    }
                    System.out.println("-----------------------------");
                    str = actionbutton.next();
                    zahl1 = (str.matches("[0-9]*")) ? Integer.parseInt(str) : -1;
                    System.out.println("-----------------------------");
                    if (!(zahl1 >= 0 && zahl1 < player.getMana().size())) {
                        System.out.println("-----------------------------");
                        System.out.println(player.getName() + " could not find the Special Attack");
                        System.out.println("-----------------------------\n");
                        break;
                    }
                    System.out.println("You've choose Item: " + player.getMana().get(zahl1).getName());
                    System.out.println("-----------------------------");
                    if (player.getMana().get(zahl1).execute(player, enemy)) {
                        System.out.println("You are a magician, well at least the Spell did not backfire..");
                    } else {
                        System.out.println("Your aren't that much of a magician.. (Spell failed)");
                    }
                    System.out.println("-----------------------------\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                    
                    break;
                case 666:
                    System.out.println("FIGHTCLUB RULES");
                    System.out.println("1st RULE: You do not talk about FIGHT CLUB.");
                    System.out.println("2nd RULE: You DO NOT talk about FIGHT CLUB.");
                    System.out.println("3rd RULE: If someone says \"stop\" or goes limp, taps out the fight is over.");
                    System.out.println("4th RULE: Only two guys to a fight.");
                    System.out.println("5th RULE: One fight at a time.");
                    System.out.println("6th RULE: No shirts, no shoes.");
                    System.out.println("7th RULE: Fights will go on as long as they have to.");
                    System.out.println("8th RULE: If this is your first night at FIGHT CLUB, you HAVE to fight.\n\n");
                    System.out.print(nerd);
                    
                    return false;

                default:
                    System.out.println("Damn noob, you have to choose the right buttons!");
            }
            if (player.isDead()) {
                return false;
            }
            if (enemy.isDead()) {
                return true;
            }
            player.apReg();
            System.out.println(enemy.getName() + " tries to attack " + player.getName());
            System.out.println(player.getName() + " has " + player.getHp() + " HP.");
            int dmgdealt = enemy.attack(player);
            
            if (dmgdealt == 0) {
                System.out.println(enemy.getName() + " is to stupid to hit you...");
            } else {
                if (player.getHp() >= 0) {
                    System.out.println(enemy.getName() + " attacks " + player.getName() 
                        + ", inflicted dmg: " + dmgdealt
                        + ", hitpoints left: " + player.getHp());
                } else {
                    System.out.println(enemy.getName() + " attacks " + player.getName() 
                        + ", inflicted dmg: " + dmgdealt
                        + ", hitpoints left: 0");
                }
                System.out.println(player.getName() + " is full of blood");
            }
            System.out.print("\n" + goon);
            enter2.nextLine();
        
        }
    }
}
