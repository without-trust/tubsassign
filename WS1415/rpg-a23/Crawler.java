import java.util.Scanner;
/** This class is supposed to manage the objects and events in our small RPG.
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */

class Crawler {

/** ascii art placed here to be accessible from inside the functions */
    private static String win = 
                      "\u001B[2J\u001B[31m\n"
                    + " ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ \n"
                    + "▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n"
                    + "▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌     ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ \n"
                    + "▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          \n"
                    + "▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ \n"
                    + "▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n"
                    + " ▀▀▀▀█░█▀▀▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ \n"
                    + "     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌       ▐░▌▐░▌     ▐░▌  ▐░▌          \n"
                    + "     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌       ▐░▌▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ \n"
                    + "     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌\n"
                    + "      ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ \n"
                    + "                                                                                   \n"
                    + "                     ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄                         \n"
                    + "                    ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌                        \n"
                    + "                     ▀▀▀▀█░█▀▀▀▀ ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀                         \n"
                    + "                         ▐░▌     ▐░▌       ▐░▌▐░▌                                  \n"
                    + "                         ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄                         \n"
                    + "                         ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌                        \n"
                    + "                         ▐░▌     ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀                         \n"
                    + "                         ▐░▌     ▐░▌       ▐░▌▐░▌                                  \n"
                    + "                         ▐░▌     ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄                         \n"
                    + "                         ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌                        \n"
                    + "                          ▀       ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀                         \n"
                    + "                                                                                   \n"
                    + " ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄  ▄▄        ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄   \n"
                    + "▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌▐░░▌      ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌  \n"
                    + "▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░▌░▌     ▐░▌▐░▌░▌     ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░▌  \n"
                    + "▐░▌       ▐░▌     ▐░▌     ▐░▌▐░▌    ▐░▌▐░▌▐░▌    ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌  \n"
                    + "▐░▌   ▄   ▐░▌     ▐░▌     ▐░▌ ▐░▌   ▐░▌▐░▌ ▐░▌   ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░▌  \n"
                    + "▐░▌  ▐░▌  ▐░▌     ▐░▌     ▐░▌  ▐░▌  ▐░▌▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌  \n"
                    + "▐░▌ ▐░▌░▌ ▐░▌     ▐░▌     ▐░▌   ▐░▌ ▐░▌▐░▌   ▐░▌ ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀ ▐░▌  \n"
                    + "▐░▌▐░▌ ▐░▌▐░▌     ▐░▌     ▐░▌    ▐░▌▐░▌▐░▌    ▐░▌▐░▌▐░▌          ▐░▌     ▐░▌   ▀   \n"
                    + "▐░▌░▌   ▐░▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌     ▐░▐░▌▐░▌     ▐░▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄   \n"
                    + "▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░▌      ▐░░▌▐░▌      ▐░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌  \n"
                    + " ▀▀       ▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀  ▀        ▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀   \n"
                    + "\u001B[0m\n";
        
    /** ascii art placed here to be accessible from inside the functions */
    private static String welcome = 
              "    ___   _____ ____    ____    ____   ____\n"
            + "   / _ \\ |  ___|  _ \\  |  _ \\  |  _ \\ / ___|  \n"
            + "  | | | || |_  | |_) | | |_) | | |_) | |  _   \n"
            + "  | |_| ||  _|_|  _ < _|  _ < _|  __/| |_| |_ \n"
            + "   \\___(_)_| (_)_| \\_(_)_| \\_(_)_| (_)\\____(_)\n\n";
    
    /** ascii art placed here to be accessible from inside the functions */
    private static String noob = 
                      "\u001B[2J\u001B[31m\n"
                    + "  ▄████  ▄▄▄       ███▄ ▄███▓▓█████     ▒█████   ██▒   █▓▓█████  ██▀███       \n"
                    + " ██▒ ▀█▒▒████▄    ▓██▒▀█▀ ██▒▓█   ▀    ▒██▒  ██▒▓██░   █▒▓█   ▀ ▓██ ▒ ██▒     \n"
                    + "▒██░▄▄▄░▒██  ▀█▄  ▓██    ▓██░▒███      ▒██░  ██▒ ▓██  █▒░▒███   ▓██ ░▄█ ▒     \n"
                    + "░▓█  ██▓░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄    ▒██   ██░  ▒██ █░░▒▓█  ▄ ▒██▀▀█▄       \n"
                    + "░▒▓███▀▒ ▓█   ▓██▒▒██▒   ░██▒░▒████▒   ░ ████▓▒░   ▒▀█░  ░▒████▒░██▓ ▒██▒     \n"
                    + " ░▒   ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░   ░ ▒░▒░▒░    ░ ▐░  ░░ ▒░ ░░ ▒▓ ░▒▓░     \n"
                    + "  ░   ░   ▒   ▒▒ ░░  ░      ░ ░ ░  ░     ░ ▒ ▒░    ░ ░░   ░ ░  ░  ░▒ ░ ▒░     \n"
                    + "░ ░   ░   ░   ▒   ░      ░      ░      ░ ░ ░ ▒       ░░     ░     ░░   ░      \n"
                    + "      ░       ░  ░       ░      ░  ░       ░ ░        ░     ░  ░   ░          \n"
                    + "                                                     ░                        \n"
                    + "\u001B[0m\n";
    /** ascii art placed here to be accessible from inside the functions */
    private static String goon = 
        "============================PRESS ENTER TO CONTINUE============================ \n";

    /** main method, that is required to start the game
     *  @param args commandline parameters
     */ 
    public static void main(String[] args) {
        Scanner go = new Scanner(System.in);
        Scanner s = new Scanner(System.in);
        Scanner enter = new Scanner(System.in);
        String movement;
        
        System.out.print(welcome);
        System.out.println("God: \tGreetings Warrior! \n" 
                + "\tWould you like to tell me your name  my little friend?");
        System.out.print("You: \tMy friends call me: ");
        
        Entity player = new Entity((int) ((Math.random() * 50) + 51), (int) ((Math.random() * 10) + 5), 
                0.65, s.next(), (int) (Math.random() * 21));
        
        System.out.println("\t\t... only I ain't got no friends.");
        System.out.println("God: \tOkay " + player.getName() + "!");
        System.out.println("\n\tJust let me see what your stats are real quick: " 
                + "\n\t\tMaximum HP:" + player.getMaxHp()
                + "\n\t\tHitpoints:" + player.getHp()
                + "\n\t\tAttack damage:" + player.getAtk()
                + "\n\t\tMana:" + player.getAP());
        System.out.println("\nGod: \tAre you ready for your first quest?");
        s.next();
        System.out.println("Great! You must go a Long way to find yourself!");
        System.out.println("");
        System.out.println("");
        System.out.print(goon);
        enter.nextLine();
        Effect heal = new Effect(8, "divine blessing", 0);
        Item pot = new Item("Healing Potion", heal);
        player.addItem(pot);
        player.addItem(pot);
        player.addItem(pot);
        player.addItem(pot);
        
        Mana ffart = new Mana("Fire-Fart", 5);
        Effect fart = new Effect(-4, "Fart", 0);
        ffart.addEffect(fart, true, true);
        player.addMana(ffart);
        Mana absorb = new Mana("Absorb Mana", 6, 0.8);
        Effect loss = new Effect(0, "loss of the blue gold", -5);
        Effect gain = new Effect(0, "gain of the blue gold", 5);
        absorb.addEffect(loss, true, true);     // success
        absorb.addEffect(gain, true, false);    // success
        absorb.addEffect(loss, false, false);   // failed
        player.addMana(absorb);
        Mana dinstant = new Mana("Instand Kill", 20, 0.5);  // create a spell with no effects 
        Effect instant = new Effect(-99999, "dead", 0);     // create Effect
        dinstant.addEffect(instant, false, false);          // defines the backfire
        dinstant.addEffect(instant, true, true);            // defines what happens when you hit correctly
        player.addMana(dinstant);
        System.out.println("\u001B[2J");

        /* initialize world */

        /*char[][] mapData = {
            {'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'F', 'X'},
            {'X', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'f', 'X'},
            {'X', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'f', 'X'},
            {'X', '.', 'm', 'm', 'm', 'm', 'm', 'f', 'm', 'm', 'm', 'm', 'f', 'f', 'X'},
            {'X', '.', '.', 'm', 'm', 'm', 'f', 'f', 'f', 'm', 'm', 'm', 'f', 'f', 'X'},
            {'X', '.', '.', '.', 'm', 'X', 'f', 'f', 'f', 'f', 'f', 'm', 'm', 'f', 'X'},
            {'X', '.', '.', '.', 'X', 'f', 'f', 'f', 'f', 'f', 'f', 'm', 'm', 'f', 'X'},
            {'X', '.', '.', 'L', 'X', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', '.', 'E', 'f', 'f', 'f', 'f', 'm', 'f', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', 'L', 'X', 'f', 'f', 'm', 'm', 'm', 'm', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', '.', '.', 'X', 'm', 'm', 'm', 'm', 'm', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', '.', '.', 'm', 'm', 'm', 'm', 'm', 'f', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', '.', 'm', 'm', 'm', 'm', 'm', 'f', 'f', 'f', 'f', 'f', 'X'},
            {'X', '.', '.', 'm', 'm', 'm', 'm', 'm', 'm', 'M', 'f', 'f', 'f', 'f', 'X'},
            {'X', 'S', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
        };

        Map m = new Map(mapData);*/

        MazeGenerator gen = new RecursiveBacktracker();
        Map m = new Map(gen.generate(6, 5));

        m.startPlayer();
        m.showWorld();
        
        System.out.println("Here are some not so important information!"
                            + "\n P = Player Position"
                            + "\n . = Field"
                            + "\n M = Mana Refuel"
                            + "\n E = Enemy"
                            + "\n f = Forest"
                            + "\n F = Final"
                            + "\n m = Mountain");
        
        boolean endofWorld = false;
        
        while (endofWorld == false) {
            do {
                movement = go.nextLine();
            } while (m.goOn(movement) == false);
            
            m.showWorld();
        
            if (m.fieldInfo(player).equals("finish")) {
                endofWorld = true;
                System.out.println(win);
            } else {
                m.showWorld();
            }

            if (player.isDead()) {
                endofWorld = true;
                System.out.println(noob);
            }
        }
    }
}
