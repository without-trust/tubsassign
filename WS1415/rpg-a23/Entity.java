import java.util.ArrayList;

/** This class describes a Entity which has hitpoints and attack damage,
 *  and has methods for healing, consumtion of Items etc...
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Entity {

    /** various private class attributes */
    private int hp, maxhp, atk, ap, maxap, apreg;
    /** player or npc name */
    private String name;
    /** hitchance */
    private double hit;
    /** inventory list */
    private ArrayList<Item> inventory = new ArrayList<Item>();
    /** list of special moves */
    private ArrayList<Mana> specialmoves = new ArrayList<Mana>();

    /** class constructor
     *  @param hp hitpoints and maximal hitpoints
     *  @param atk attack damage
     *  @param hit hitchance
     *  @param name player or npc name
     *  @param ap ability pionts
     */
    public Entity(int hp, int atk, double hit, String name, int ap) {
        this.hp = hp;
        this.maxhp = hp;
        this.atk = atk;
        this.hit = hit;
        this.name = name;
        this.ap = ap;
        this.maxap = ap;
        this.apreg = 2;
    }

    /** set hitpoints
     *  @param hp Hitpoints
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /** get hitpoints
     *  @return hitpoints
     */
    public int getHp() {
        return this.hp;
    }

    /** getter for maximal hitpoints
     *  @return maximal hitpoints
     */
    public int getMaxHp() {
        return this.maxhp;
    }

    /** setter for maximal hitpoints
     *  @param maxhp maximal amout of hitpoints
     */
    public void setMaxHp(int maxhp) {
        this.maxhp = maxhp;
    }

    /** set attack damage
     *  @param atk attack damage
     */
    public void setAtk(int atk) {
        this.atk = atk;
    }

    /** get attack damage
     *  @return attack damage
     */
    public int getAtk() {
        return this.atk;
    }

    /** get hitchance
     *  @return hitchance
     */
    public double getHit() {
        return this.hit;
    }

    /** set hitchance
     *  @param hit hitchance
     */
    public void setHit(double hit) {
        this.hit = hit;
    }

    /** get character name
     * @return character name
     */
    public String getName() {
        return this.name;
    }

    /** set character name
     *  @param name character name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** set AP
     * @param ap Specialattacks
     */
    public void setAP(int ap) {
        this.ap = ap;
    }
    
    /** get AP
     * @return AP Points
     */
    public int getAP() {
        return this.ap;
    }
    
    /** set MaxAP
     * @param maxap 
     */
    public void setMaxAP(int maxap) {
        this.maxap = maxap;
    }
    
    /** get MaxAP
     * @return MaxAP Points
     */
    public int getMaxAP() {
        return this.maxap;
    }
    
    /** set AP Regeneration
     * @param apreg 
     */
    public void setAPReg(int apreg) {
        this.apreg = apreg;
    }
    
    /** get AP Regeneration
     * @return current geneneration of ap 
     */
    public int getAPRegen() {
        return this.apreg;
    }
    
    /** attacking another entity
     *  @param enemy entity to attack
     *  @return how much dmg was dealt
     */
    public int attack(Entity enemy) {
        /* Due to a lack of a true random number generator,
         * Math.random() is used to generate pseudo-random numbers instead. */
        int inflict = (Math.random() < this.hit) ? (int) ((Math.random() + 1) * this.atk) : 0;
        enemy.damage(inflict);
        return inflict;
    }

    /** inflict damage to character and tell wether or not it's
     *  "still alive" afterwards
     *  @param dmg amount of damage inflicted
     *  @return wether or not the character is alive
     */
    public boolean damage(int dmg) {
        this.hp -= dmg;
        
        return !this.isDead();
    }

    /** returns wether or not the character is already dead
     *  @return if character is dead
     */
    public boolean isDead() {
        return (this.hp <= 0) ? true : false;
    }
    
    /** Method will heal with most effective healing item
     *  @return wether or not the entity was healt
     */
    public boolean heal() {
        Item pot = this.bestHealingItem();
        if (pot.getId() != -1) {
            this.hp += pot.getEffect().getHp();
            if (this.hp > this.maxhp) {
                this.hp = this.maxhp;
            }
            this.ap += pot.getEffect().getAp();
            if (this.ap > this.maxap) {
                this.ap = this.maxap;
            }
            this.removeItem(pot);
            return true;
        } else {
            return false;
        }
    }

    /** method to consume an item
     *  @param item item to consume
     *  @return wether item was cunsumed or not
     */
    public boolean consumeItem(Item item) {
        if (this.hasItem(item)) {
            this.hp += item.getEffect().getHp();
            if (this.hp > this.maxhp) {
                this.hp = this.maxhp;
            }
            this.removeItem(item);
            return true;
        } else {
            return false;
        }
    }

    /** applys any efffect to the player 
     *  @param e Effect to be applied
     */
    public void applyEffect(Effect e) {
        this.hp += e.getHp();
        if (this.hp > this.maxhp) {
            this.hp = this.maxhp;
        }
        this.ap += e.getAp();
        if (this.ap > this.maxap) {
            this.ap = this.maxap;
        }
    }


    /** applys any efffect to any entity
     *  @param e Effect to be applied
     *  @param entity entity that will be affected
     */
    public void applyEffect(Effect e, Entity entity) {
        entity.hp += e.getHp();
        if (entity.hp > entity.maxhp) {
            entity.hp = entity.maxhp;
        }
        entity.ap += e.getAp();
        if (entity.ap > entity.maxap) {
            entity.ap = entity.maxap;
        }
    }
    
    /** search inventory for most effective healing Item and return it
     *  @return most effective healing item
     */
    private Item bestHealingItem() {
        int maxheal = 0;
        Item perfect = new Item(-1);

        if (this.inventory.size() > 0) {
            for (Item item : this.inventory) {
                if (maxheal < item.getEffect().getHp()) {
                    maxheal = item.getEffect().getHp();
                    perfect = item;
                }
            }
        }

        return perfect;
    }

    /** returns inventory as a List of Items
     *  @return inventory as a Item list
     */
    public ArrayList<Item> getInventoryList() {
        return this.inventory;
    }

    /** returns inventory as a List of UNIQUE Items
     *  @return inventory
     */
    public ArrayList<Item> getInventory() {
        ArrayList<Item> unique = new ArrayList<Item>();
        for (Item item: this.getInventoryList()) {
            if (!unique.contains(item)) {
                unique.add(item);
            }
        }
        return unique;
    }

    /** add an Item to the inventory
     *  @param item Item to be added to the inventory
     */
    public void addItem(Item item) {
        this.inventory.add(item);
    }

    /** remove an item from inventory
     *  @param item Item to be removed from Inventory list
     */
    public void removeItem(Item item) {
        this.inventory.remove(item);
    }

    /** remove an item from inventory
     *  @param index index of item in inventory list
     */
    public void removeItem(int index) {
        this.inventory.remove(index);
    }

    /** returns how many items of the one specified are available
     *  @param item Item to check for in inventory
     *  @return how many times the item is in the inventory
     */
    public int countItem(Item item) {
        int count = 0;
        for (Item item1 : this.inventory) {
            if (item == item1) {
                count++;
            }
        }
        return count;
    }
    
    /** check wether or not Item is present at least once in inventory
     *  @param item Item to check for in inventory
     *  @return wether or not Item is in inventory
     */
    public boolean hasItem(Item item) {
        for (Item item1 : this.inventory) {
            if (item == item1) {
                return true;
            }
        }
        return false;
    }

    /** adding a Mana(that is basically a spell)
     *  @param mana Mana to be added to the object
     */
    public void addMana(Mana mana) {
        this.specialmoves.add(mana);
    }


    /** simple getter for list of spells
     *  @return list of spells
     */ 
    public ArrayList<Mana> getMana() {
        return this.specialmoves;
    }

    /** Method for regenerating AP
     *  @return renerated hp
     */
    public int apReg() {
        int reg = this.maxap - this.ap;
        if (reg >= 2) {
            this.ap += this.apreg;
            return this.apreg;
        } else {
            this.ap += reg;
            return reg;
            
        }
        
    }   
}
