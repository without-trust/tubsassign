/** An Item is supposed to be used or consumed by an Entity,
 *  
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Item {
    /** ID */
    private int id;
    /** Itemname */
    private String name;
    /** Effect */
    private Effect effect;

    /** Constructor
     *  @param id ID
     */
    Item(int id) {
        this.id = id;
    }

    /** Constructor
     *  @param name Itemname
     */
    Item(String name) { 
        this.name = name;
        this.effect = new Effect();
    }

    /** Constructor
     *  @param name Itemname
     *  @param effect add effect to Item
     */
    Item(String name, Effect effect) {
        this.name = name;
        this.effect = effect;
    }
    
    /** returns item id
     *  @return item id
     */
    public int getId() {
        return this.id;
    }

    /** setter for item id
     *  @param id item id
     */
    public void setId(int id) {
        this.id = id;
    }

    /** returns effect name
     *  @return effect name
     */
    public String getName() {
        return this.name;
    }

    /** setter for effect name
     *  @param name of effect
     */
    public void setName(String name) {
        this.name = name;
    }

    /** returns effect of item
     *  @return effect of item
     */
    public Effect getEffect() {
        return this.effect;
    }

    /** set effect for item 
     *  @param effect effect to be set for item
     */
    public void setEffect(Effect effect) {
        this.effect = effect;
    }
}
