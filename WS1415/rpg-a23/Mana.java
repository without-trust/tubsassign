
import java.util.ArrayList;

/** Mana is a class which can hold a spell with multiple effects...
 *
 *      Hints:
 *      - direction == true     - means effect is supposed to be applied to enemy
 *      - direction == false    - means effect is supposed to be applied to player
 *
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */

public class Mana {
    
    /** defines the required amount of AP */
    private int cost;
    
    /** hitchance defines how likely it is to hit */
    private double hit;
    
    /** name to be displayed*/
    private String name;

    /** effects to be applied in case of success */
    private ArrayList<Effect> effects_list_success = new ArrayList<Effect>();
    /** direction of successfull spells */
    private ArrayList<Boolean> effect_directions_success = new ArrayList<Boolean>();
    /** effects to be applied in case of failure */
    private ArrayList<Effect> effects_list_fail = new ArrayList<Effect>();
    /** direction of successfull spells */
    private ArrayList<Boolean> effect_directions_fail = new ArrayList<Boolean>();
    
    
    /** Constructor
     *  @param name Name of Mana
     *  @param cost how much will casting the spell cost
     */
    Mana(String name, int cost) {
        this.name = name;
        this.cost = cost;
        this.hit = 1.0;
    }

    /** Constructor
     *  @param name Name of Mana
     *  @param cost how much will casting the spell cost
     *  @param hit define hit chance of spell
     */
    Mana(String name, int cost, double hit) {
        this.name = name;
        this.cost = cost;
        this.hit = hit;
    }

    /** simply sets name
     *  @param name you really should know what this does
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** getter for name attribute
     *  @return mana's name
     */
    public String getName() {
        return this.name;
    }
    
    /** simple setter for cost
     *  @param cost defines the required AP to cast the spell
     */
    public void setCost(int cost) {
        this.cost = cost;
    }
    
    /** getter for AP cost
     * @return AP cost per use
     */
    public int getCost() {
        return this.cost;
    }
    
    
    /** Adds another effect to the Mana
     *  @param effect Effect to be added
     *  @param success is the effect supposed to be applied in case of success or failure
     *  @param direction which direction the effect will be applied
     */
    public void addEffect(Effect effect, boolean success, boolean direction) {
        if (success) {
            this.effects_list_success.add(effect);
            this.effect_directions_success.add(direction);
        } else {
            this.effects_list_fail.add(effect);
            this.effect_directions_fail.add(direction);
        }
    }
    
    /** Executes a Mana
     *  @param player Entity casting the spell
     *  @param enemy Entity that may be effected by the spell
     *  @return wether or not the spell was successful or backfired
     * */
    public boolean execute(Entity player, Entity enemy) {
        boolean hit;
        double rand = Math.random();
    
        ArrayList<Effect> effects_list = new ArrayList<Effect>();
        ArrayList<Boolean> effect_directions = new ArrayList<Boolean>();

        // compare hitchance to rand, to determine wether or nit the spell will be successfull
        if (this.hit >= rand) {
            hit = true;     // used for return 
            effects_list = this.effects_list_success;
            effect_directions = this.effect_directions_success;
        } else {
            hit = false;    // used for return
            effects_list = this.effects_list_fail;
            effect_directions = this.effect_directions_fail;
        }

        int count = effects_list.size();

        for (int i = 0; i < count; i++) {
            //System.out.println(i + ". " + effects_list.get(i).getName());
            if (effect_directions.get(i)) {
                // casted on enemy
                //System.out.println("casted on enemy");
                enemy.applyEffect(effects_list.get(i));
            } else {
                // casted on player
                //System.out.println("casted on player");
                player.applyEffect(effects_list.get(i));
            }
        }

        return hit;
    }
}
