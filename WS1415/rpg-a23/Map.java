import java.awt.Point;
import java.util.ArrayList;
/** management of map
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public class Map {
    /** Generator for the Enemy
     */
    private int fi = (int) (Math.random() * 2 + 1);
    
    /**This next lines will generate the Position of the player at this gamemap
     */
    private char[][] world;
    /**
     * look at line 12
     */
    private Point position = new Point();
    /*
    *at this point this class map get the map
    */
    public Map(char[][] mapData) {
        this.world = mapData;
    }
    
    public void startPlayer() {
        for (int x = 0; x < this.world.length; x++) {
            for (int y = 0; y < this.world[x].length; y++) {
                if (this.world[x][y] == 'S') {
                    this.position.setLocation(x, y);
                    this.world[x][y] = ' ';
                }
            }
        }
    }
    
    public void showWorld() {
        System.out.println("\u001B[2J");
        for (int x = 0; x < this.world.length; x++) {
            for (int y = 0; y < this.world[x].length; y++) {
                if (x == this.position.getX() && y == this.position.getY()) {
                    System.out.print('P');
                } else {
                    System.out.print(this.world[x][y]);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println("");
    }
    
    public boolean goOn(String direction) {
        boolean goonPlayer = true;
        
        // using chars to support java6, legacy support is important ;)
        char cdirection = 'z';
        try {
            cdirection = direction.charAt(0);
        } catch (Exception e) {
            // do something, lallaa
        }

        switch (cdirection) {
            case 'w':
                if (this.world[(int) this.position.getX() - 1][(int) this.position.getY()] == 'X' 
                        || this.world[(int) this.position.getX() - 1][(int) this.position.getY()] == 'm') {
                    System.out.println("Wrong Way!");
                    goonPlayer = false;
                } else {
                    this.position.setLocation(this.position.getX() - 1, this.position.getY());
                } 
                break;
            case 'a':
                if (this.world[(int) this.position.getX()][(int) this.position.getY() - 1] == 'X' 
                        || this.world[(int) this.position.getX()][(int) this.position.getY() - 1] == 'm') {
                    System.out.println("Wrong Way!");
                    goonPlayer = false;
                } else {
                    this.position.setLocation(this.position.getX(), this.position.getY() - 1);
                } 
                break;
            case 's':
                if (this.world[(int) this.position.getX() + 1][(int) this.position.getY()] == 'X'
                        || this.world[(int) this.position.getX() + 1][(int) this.position.getY()] == 'm') {
                    System.out.println("Wrong Way!");
                    goonPlayer = false;
                } else {
                    this.position.setLocation(this.position.getX() + 1, this.position.getY());
                } 
                break;
            case 'd':
                if (this.world[(int) this.position.getX()][(int) this.position.getY() + 1] == 'X'
                    || this.world[(int) this.position.getX()][(int) this.position.getY() + 1] == 'm') {
                    System.out.println("Wrong Way!");
                    goonPlayer = false;
                } else {
                    this.position.setLocation(this.position.getX(), this.position.getY() + 1);
                } 
                break;
            default:
                System.err.println("Use the right keys to move [W,A,S,D]");
        }
        return goonPlayer;
    }
    
    public String fieldInfo(Entity player) {
        String enteredPlace = "";
        switch (this.world[(int) this.position.getX()][(int) this.position.getY()]) {
            case 'f':
                if (fi == 1) {
                    int en = (int) (Math.random() * 4);
                    ArrayList<Entity> enemys = new ArrayList<Entity>();
                    enemys.add(new Entity(50, 6, 0.5, "Hetler", 0));
                    enemys.add(new Entity(60, 7, 0.5, "GLaDOS", 0));
                    enemys.add(new Entity(70, 8, 0.6, "Bowser", 0));
                    enemys.add(new Entity(1000, 9, 0.8, "God", 0));
                    
                    Entity enemy = enemys.get(en);
                    
                    System.out.println("You walk through the bushes and someone attacks you");
                    Fightclub.battle(player, enemy);
                    
                } else {
                    System.out.println("You walk on a small way between the trees");
                }
                break;
            case 'L':
                System.out.println("Dear traveler,\n You must fight against these enemy to find the Goal");
                break;
            case 'E':
                int en = (int) (Math.random() * 4);
                ArrayList<Entity> enemys = new ArrayList<Entity>();
                enemys.add(new Entity(50, 6, 0.5, "Hetler", 0));
                enemys.add(new Entity(60, 7, 0.5, "GLaDOS", 0));
                enemys.add(new Entity(70, 8, 0.6, "Bowser", 0));
                enemys.add(new Entity(1000, 9, 0.8, "God", 0));
                    
                Entity enemy = enemys.get(en);
                    
                Fightclub.battle(player, enemy);
                break;
            case 'M':
                if (player.getAP() <= player.getMaxAP()) {
                    System.out.println("Your Mana grows up!");
                    player.setAP(20);
                } else {
                    System.out.println("You have the maximum of Mana!");
                }
                break;
            case 'F':
                enteredPlace = "finish";
                break;
            default:
                break;
        }
        return enteredPlace;
        
    }
    
}
