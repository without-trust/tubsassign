/** Interface for generating mazes
 *  @author Daniel Thümen   4523176 Gruppe 7a
 *  @author Philipp Bartsch 4569839 Gruppe 7a
 */
public interface MazeGenerator {
    /** attribute */
    char WALL = 'X';    //#
    /** attribute */
    char FREE = '.';    //.
    /** attribute */
    char START = 'S';   //S
    /** attribute */
    char BATTLE = 'E';  //B
    /** attribute */
    char SMITHY = '.';  //T
    /** attribute */
    char WELL = 'M';    //O
    /** attribute */
    char GOAL = 'F';    //Z
    /** method to handle the overall generation process
     * @param width width of the maze to be generated
     * @param height height of the maze to be generated
     * @throws IllegalArgumentException in case the
     *      dimensions are fucked up
     * @return simple char array that contains a map..
     */
    char[][] generate(int height, int width);
    
    /** getter for time(in millis) needed to generate raw mapdata
     * @return time
     */
    long getTime();
}
