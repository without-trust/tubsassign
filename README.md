tubsassign
==========

CS homeworks will be uploaded here...     
...at least it is the plan :)

Dependencies:
- working java development environment (the FREE version is strongly recommended)
- checkstyle 5.7 (run 'make setup' to download binary to misc/checkstyle-5.7-all.jar)
    - downloading via makefile currently requires wget
